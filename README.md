English: [README_EN.md](README_EN.md).

# Скрипты для MetaTrader

Если не указано другое, код доступен как для MT4, так и для MT5.

Далее под папкой `MQL` предполагается папка `MQL4` или `MQL5` в зависимости от версии терминала.

Скрипты, для которых есть версии для MT4 и MT5, могут быть идентичными по содержанию. Приходится использовать отдельные файлы, так как MetaTrader распознаёт версию MQL исключительно по расширению файла.

## Установка

Код индикатора или скрипта обычно состоит из пары основных файлов, по одному для MQL4 и MQL5 (с расширениями .mq4 и .mq5), и папки с включаемыми файлами (`...-include`).

Для установки достаточно скопировать папку, содержащую эти файлы и папку в одно место, для индикаторов это `MQL/Indicators`. Не нужно выносить папку `...-include` в папку `MQL/Includes`, т.к. в коде используются относительные пути, и такой способ не будет работать.

Папка с включаемыми файлами не будет мешать в MetaTrader, т.к. в ней нет основного кода индикатора, и в структуре папок в Навигаторе она не появится. Также можно не удалять "лишний" основной файл от другой версии MQL, этот файл также будет проигнорирован в Навигаторе MT.

Например, чтобы установить индикатор VP достаточно скопировать папку `MQL/Indicators/VP` из этого хранилища в папку `MQL4/Indicators/VP` и/или `MQL5/Indicators/VP`. Повторюсь, файлы VP.mq4 и VP.mq5 и папка `VP-include` должны находится в одной папке. Чтобы не было лишней папки-уровня в Навигаторе, можно копировать содержимое `MQL/Indicators/VP` непосредственно в `MQL5/Indicators`.

Ниже показаны варианты корректного расположения файлов и папок после установки двух индикаторов - VP и Index.

Как есть (в хранилище) для MQL5:

```ini
[MQL5]
    [Indicators]
        [VP]
            [VP-include]
            VP.mq4
            VP.mq5
        [Index]
            [Index-include]
            Index.mq4
            Index.mq5
```

Файлы `.mq4` здесь можно было убрать, но они не будут мешать, можно оставлять.

Без лишней папки в Навигаторе для 4 и 5:

```ini
...
[MQL4]
    [Indicators]
        [Index-include]
        [VP-include]
        Index.mq4
        Index.mq5
        VP.mq4
        VP.mq5
...
[MQL5]
    [Indicators]
        [Index-include]
        [VP-include]
        Index.mq4
        Index.mq5
        VP.mq4
        VP.mq5
```

Чтобы узнать, где находится папка `MQL4` или `MQL5` в MetaTrader или MetaEditor можно вызвать меню `Файл/Открыть каталог данных`.

## Скрипты

### ColorManager

Скрипт для управления цветовой схемой графика. Подробнее: <https://www.fxcoder.ru/2012/08/colormanager-script.html>.

При первом запуске будет создан файл с небольшим набором цветовых схем. Цветовые схемы расположены в файле `MQL/Files/ColorManager/colors.ini`.

### FindVL

Скрипт показывает отработку уровней максимальных объёмов распределений профиля объёмов. Подробнее: <https://www.fxcoder.ru/2019/11/findvl-script.html>.

### ScrollMaster

Скрипт синхронизирует смещение всех графиков в терминале с тем, на котором он запущен. Подробнее: <https://www.fxcoder.ru/2019/11/scrollmaster.html>.

## Индикаторы

### BetterChart

Индикатор предназначен для улучшения функций стандартного графика. Подробнее: <https://www.fxcoder.ru/2019/12/betterchart-indicator.html>

### Chart

Индикатор для отображения графика в виде свечек в дополнительном окне. Подробнее: <https://www.fxcoder.ru/2011/06/chart-indicator.html>.

### Index

Индекс на основе среднего геометрического. Подробнее: <https://www.fxcoder.ru/2011/06/index-indicator.html>.

### MultiStoch

Несколько индикаторов Stochastic в одном окне. Подробнее: <https://www.fxcoder.ru/2012/05/multistoch-indicator.html>.

### VP

Распределение сделок по ценовым уровням на заданном временном участке. Подробнее: <https://www.fxcoder.ru/2009/09/volume-profile-indicator.html>.

<!--

### EA-Line и EA-Line0

Набор индикаторов для построения графиков по данным торговых роботов при визуальном тестировании в MetaTrader. Подробнее: <https://www.fxcoder.ru/2013/07/ea-line-indicator.html>.

### MA

Скользящая средняя (Moving Average) с индикацией касания цены. Подробнее: <https://www.fxcoder.ru/2011/06/ma-indicator.html>.

### StdScore

Расчет стандартной оценки и вывод ее в виде свечей. Подробнее: <https://www.fxcoder.ru/2013/07/stdscore-indicator.html>. Только MT4.
-->
