/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "ColorManager 3.0.6. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict

#include "ColorManager-include/bsl.mqh"
#include "ColorManager-include/class/ini_file.mqh"

string prefix_ = "cm_";
CBGO *buttons_[]; // `_go` controls the pointers
CBGO edit_save_name_(prefix_ + "edit_save_name");
CBGO button_save_(prefix_ + "button_save");
CBGO button_close_(prefix_ + "button_close");

const string file_path_               = "ColorManager\\colors.ini";
const int    file_flags_              = FILE_UNICODE; // add ` | FILE_COMMON` if you want to use the common dir
const bool   file_common_flag_        = _flag.check(file_flags_, FILE_COMMON);
const color  color_button_            = clrBlack;
const color  color_button_back_       = clrGainsboro;
const color  color_button_down_back_  = clrGray;
const color  color_button_delete_     = clrRed;
const color  color_edit_back_         = clrWhite;

const string ini_header[] =
{
	"; ColorManager colors",
	""
};

const string ini_standard_colors[7 * (1 + 12 + 1) - 1] =
{
	"[black on gray]",
	"ask=clrSilver",          "background=clrLightGray",  "bid=clrSilver",        "candle_bear=clrBlack",
	"candle_bull=clrWhite",   "chart_down=clrBlack",      "chart_line=clrBlack",  "chart_up=clrBlack",
	"foreground=clrBlack",    "grid=clrSilver",           "last=clrBlack",        "stop_level=clrOrangeRed",
	"",
	"[dark soft 2]",
	"ask=clrCrimson",         "background=clrBlack",      "bid=0,64,128",         "candle_bear=112,67,67",
	"candle_bull=71,116,67",  "chart_down=236,166,116",   "chart_line=clrWhite",  "chart_up=186,208,148",
	"foreground=96,96,96",    "grid=48,48,48",            "last=clrBlack",        "stop_level=clrOrangeRed",
	"",
	"[dark soft]",
	"ask=clrCrimson",         "background=clrBlack",      "bid=0,64,128",         "candle_bear=96,64,64",
	"candle_bull=64,96,64",   "chart_down=224,128,128",   "chart_line=96,96,64",  "chart_up=128,224,128",
	"foreground=96,96,96",    "grid=48,48,48",            "last=clrBlack",        "stop_level=clrOrangeRed",
	"",
	"[orange]",
	"ask=clrCrimson",         "background=clrBlack",      "bid=0,64,128",           "candle_bear=clrOrangeRed",
	"candle_bull=17,17,17",   "chart_down=clrOrangeRed",  "chart_line=clrYellow",   "chart_up=clrGold",
	"foreground=96,96,96",    "grid=clrChocolate",        "last=clrBlack",          "stop_level=clrRed",
	"",
	"[bright gray]",
	"ask=clrCrimson",           "background=clrLightGray",  "bid=0,64,128",         "candle_bear=224,128,128",
	"candle_bull=128,192,160",  "chart_down=clrFireBrick",  "chart_line=clrBlack",  "chart_up=0,128,64",
	"foreground=clrBlack",      "grid=clrGray",             "last=clrBlack",        "stop_level=clrOrangeRed",
	"",
	"[gray]",
	"ask=clrCrimson",                 "background=clrLightGray",  "bid=0,64,128",         "candle_bear=199,163,163",
	"candle_bull=clrLightSteelBlue",  "chart_down=clrMaroon",     "chart_line=clrBlack",  "chart_up=clrDarkSlateGray",
	"foreground=clrBlack",            "grid=clrGray",             "last=clrBlack",        "stop_level=clrOrangeRed",
	"",
	"[red & green]",
	"ask=128,0,64",         "background=clrBlack",  "bid=0,64,128",           "candle_bear=64,0,0",
	"candle_bull=0,64,0",   "chart_down=clrRed",    "chart_line=clrYellow",   "chart_up=clrLime",
	"foreground=96,96,96",  "grid=48,48,48",        "last=clrBlack",          "stop_level=clrOrangeRed"
};

void OnStart()
{
	// save foreground state
	bool chart_foreground = _chart.foreground();
	_chart.foreground(false);

	// Interface restart loop (after changes)
	bool need_restart = true;
	
	while (need_restart)
	{
		ArrayFree(buttons_);

		// Read colors

		// use standard colors on first run
		if (!_file.exists(file_path_, file_common_flag_))
		{
			string lines[];
			_arr.add_array(lines, ini_header);
			_arr.add_array(lines, ini_standard_colors);
			_file.write_lines(file_path_, file_flags_, lines);
			Print("First use of " + _mql.program_name() + ". Default color schemes was saved in file: " + file_path_);
		}
		
		CIniFile ini;
		ini.load(file_path_, file_flags_);
		string sections[];
		int nsections = ini.get_sections(sections);
		_arr.sort_text(sections);
		
		
		// Add buttons
		
		int x = 10;
		int y = 20;
		
		// schemes buttons
		for (int i = 1; i < nsections; i++)
		{
			if (sections[i] == "")
				continue;
		
			// For each scheme add two buttons: select and delete
			color fg_color = color_button_;
			color bg_color = color_button_back_;
			
			add_button(prefix_ + sections[i], sections[i], x + 30, y, 100, 20).colour(fg_color).bg_color(bg_color);
			add_button(prefix_ + sections[i] + "?delete", "X", x, y, 20, 20).colour(color_button_delete_);
			y += 25;
			
			if (y > _chart.height_in_pixels() - 50)
			{
				x += 150;
				y = 20;
			}
		}

		// controls

		edit_save_name_.recreate(OBJ_EDIT, 0, 0, 0);
		edit_save_name_.xy(x + 30, y).xy_size(100, 20);
		edit_save_name_.colour(color_button_).bg_color(color_edit_back_);
		edit_save_name_.text("enter name...").font_size(8).tooltip_disable();
		edit_save_name_.back(false).zorder_max();

		button_save_.recreate(OBJ_BUTTON, 0, 0, 0);
		button_save_.xy(x + 140, y).xy_size(50, 20);
		button_save_.colour(color_button_).bg_color(color_button_back_);
		button_save_.text("Save").font_size(8).tooltip_disable();
		button_save_.back(false).zorder_max();

		button_close_.recreate(OBJ_BUTTON, 0, 0, 0);
		button_close_.xy(x + 200, y).xy_size(50, 20);
		button_close_.colour(color_button_).bg_color(color_button_back_);
		button_close_.text("Close").font_size(8).tooltip_disable();
		button_close_.back(false).zorder_max();

		ChartRedraw();
		
		// array of buttons will not be changed
		int nbuttons = ArraySize(buttons_);

		need_restart = false;

		// Interface loop (waiting for user command)
		while(!IsStopped())
		{
			// Check buttons state
			for (int i = 0; i < nbuttons; i++)
			{
				if (buttons_[i].state())
				{
					string name = buttons_[i].name();

					// select or delete
					if (_str.contains(name, "?delete"))
					{
						// Get file name from button name
						StringReplace(name, "?delete", "");
						name = StringSubstr(name, StringLen(prefix_));
						
						delete_colors(name);

						need_restart = true;
						visible_button_release(buttons_[i]);
						break;
					}
					else
					{
						load_colors(buttons_[i].text());
						edit_save_name_.text(buttons_[i].text());

						visible_button_release(buttons_[i]);
						break;
					}
				}
			}

			// Restart after deletion
			if (need_restart)
				break;

			// Save
			if (button_save_.state())
			{
				save_colors(edit_save_name_.text());
				visible_button_release(button_save_);
				need_restart = true;
				break;
			}

			// Close => no restart
			if (button_close_.state())
				break;
			
			// to avoid high cpu load
			Sleep(20);
		}
		
		// Remove buttons
		
		for (int i = ArraySize(buttons_) - 1; i >= 0; i--)
			buttons_[i].del();

		edit_save_name_.del();
		button_save_.del();
		button_close_.del();
	}
	
	// restore foreground state
	_chart.foreground(chart_foreground);
}

// Visible (slower) button release. See mki#14
void visible_button_release(CBGO &button)
{
	color orig_bg_color = button.bg_color();
	button.bg_color(color_button_down_back_);
	_chart.redraw();
	Sleep(100);
	button.bg_color(orig_bg_color);
	button.state(false);
	_chart.redraw();
}

CBGO *add_button(string name, string text, int x, int y, int w, int h)
{
	// tooltip = "\n" => disable tooltip
	return(add_button(name, text, x, y, w, h, "\n"));
}

CBGO *add_button(string name, string text, int x, int y, int w, int h, string tooltip)
{
	CBGO *button = _go[name];
	button.recreate(OBJ_BUTTON, 0, 0, 0);
	button.xy(x, y).xy_size(w, h);
	button.colour(color_button_).bg_color(color_button_back_);
	button.text(text).font_size(8);
	button.zorder_max().back(false);
	button.tooltip(tooltip);

	_arr.add_ref(buttons_, button);
	return(button);
}

bool load_colors(string name)
{
	CIniFile ini;
	if (!ini.load(file_path_, file_flags_))
		return(false);

	_chart.color_ask         (ini.get_value("ask",         name, _chart.color_ask()));
	_chart.color_background  (ini.get_value("background",  name, _chart.color_background()));
	_chart.color_bid         (ini.get_value("bid",         name, _chart.color_bid()));
	_chart.color_candle_bear (ini.get_value("candle_bear", name, _chart.color_candle_bear()));
	_chart.color_candle_bull (ini.get_value("candle_bull", name, _chart.color_candle_bull()));
	_chart.color_chart_down  (ini.get_value("chart_down",  name, _chart.color_chart_down()));
	_chart.color_chart_line  (ini.get_value("chart_line",  name, _chart.color_chart_line()));
	_chart.color_chart_up    (ini.get_value("chart_up",    name, _chart.color_chart_up()));
	_chart.color_foreground  (ini.get_value("foreground",  name, _chart.color_foreground()));
	_chart.color_grid        (ini.get_value("grid",        name, _chart.color_grid()));
	_chart.color_last        (ini.get_value("last",        name, _chart.color_last()));
	_chart.color_stop_level  (ini.get_value("stop_level",  name, _chart.color_stop_level()));
	_chart.color_volume      (ini.get_value("volume",      name, _chart.color_volume()));

	_chart.redraw();
	return(true);
}

void save_colors(string name)
{
	CIniFile ini;
	ini.load(file_path_, file_flags_);

	ini.set_value("ask",         name, _chart.color_ask());
	ini.set_value("background",  name, _chart.color_background());
	ini.set_value("bid",         name, _chart.color_bid());
	ini.set_value("candle_bear", name, _chart.color_candle_bear());
	ini.set_value("candle_bull", name, _chart.color_candle_bull());
	ini.set_value("chart_down",  name, _chart.color_chart_down());
	ini.set_value("chart_line",  name, _chart.color_chart_line());
	ini.set_value("chart_up",    name, _chart.color_chart_up());
	ini.set_value("foreground",  name, _chart.color_foreground());
	ini.set_value("grid",        name, _chart.color_grid());
	ini.set_value("last",        name, _chart.color_last());
	ini.set_value("stop_level",  name, _chart.color_stop_level());
	
	// add header and save
	string lines[];
	ini.get_lines(lines);
	_arr.insert_array(lines, ini_header, 0);
	_file.write_lines(file_path_, file_flags_, lines);
}

bool delete_colors(string name)
{
	CIniFile ini;
	if (!ini.load(file_path_, file_flags_))
		return(false);
	
	if (!ini.delete_section(name))
		return(false);

	// add header and save
	string lines[];
	ini.get_lines(lines);
	_arr.insert_array(lines, ini_header, 0);
	return(_file.write_lines(file_path_, file_flags_, lines));
}

/*
Last changes:

3.0:
	* fixed: chart may overlap buttons
	* storage of all colors in one file
	* a list of default color schemes is created on first launch
	* unified code MQL4/MQL5
	* unicode support in scheme name
	* any character in scheme name (no restrictions as in the path to the file)
	* the ability to use more schemes (display in multiple columns)
*/
