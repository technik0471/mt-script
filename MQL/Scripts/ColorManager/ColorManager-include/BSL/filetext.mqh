/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к текстовому файлу. Better Standard Library. © FXcoder
// Файл через этот класс нельзя явно закрыть и переоткрыть. Открытие файла происходит при объявлении или создании через new,
// закрытие при уничтожении объекта (delete или при выходе за область видимости).

#property strict

#include "util/arr.mqh"
#include "util/flag.mqh"
#include "file.mqh"


class CBFileText: public CBFile
{
private:

	int char_size_;


public:

	void CBFileText(string path):
		CBFile(path)
	{
	}

	virtual bool open(int flags) override
	{
		flags_ = FILE_TXT | flags;
		
		// FILE_ANSI | FILE_UNICODE => Unicode
		// FILE_ANSI                => ANSI
		//             FILE_UNICODE => Unicode
		//                          => Unicode
		char_size_ = _flag.check(flags_, FILE_ANSI) && !_flag.check(flags_, FILE_UNICODE) ? 1 : 2;
		return(CBFile::open(flags_));
	}

	bool open_write_ansi(int flags = 0)
	{
		return(open(FILE_WRITE | FILE_ANSI | flags));
	}

	bool open_write_ansi_common(int flags = 0)
	{
		return(open_write_ansi(FILE_COMMON | flags));
	}

	// См. параметры для FileWriteString, параметр length на практике не нужен
	uint write_line(string s)
	{
		return(::FileWrite(handle_, s)); //TODO: убрать этот вариант, при сохранении double почему-то убирается дробная часть
	}

	// См. параметры для FileWriteString, параметр length на практике не нужен
	uint write_string(string s)
	{
		return(::FileWriteString(handle_, s));
	}

	// См. параметры для FileReadString, параметр length на практике не нужен
	string read_line()
	{
		return(::FileReadString(handle_));
	}

	//TODO: test (ansi, unicode)
	// Сохранить массив строк в текстовый файл построчно.
	bool write_lines(const string &lines[])
	{
		for (int i = 0, count = ::ArraySize(lines); i < count; i++)
		{
			string line = lines[i];
			uint size = char_size_ * (::StringLen(line) + 2); // 2 is "\r\n"
			uint written_size = write_line(line);
			
			if (written_size != size)
				return(false);
		}
		
		return(true);
	}

	int read_lines(string &lines[])
	{
		int reserve = 1024;
		::ArrayResize(lines, 0, reserve);

		while (!is_ending())
		{
			string line = read_line();
			
			if (_arr.add(lines, line, reserve) < 0)
				return(-1);
		}
		
		return(::ArraySize(lines));
	}

};
