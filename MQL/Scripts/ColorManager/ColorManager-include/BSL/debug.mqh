/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

/*
Отладка. Better Standard Library. © FXcoder
Работа зависит от определений DEBUG, DEBUG_WARNING_PRINT, DEBUG_WARNING_ALERT.
*/

#property strict

// В режиме отладки терминала включать макрос отладки автоматически
#ifdef _DEBUG
	#ifndef DEBUG
		#define DEBUG
	#endif
#endif


#define FUNC_LINE_PREFIX      (string(__FUNCTION__) + "," + string(__LINE__) + ": ")
#define ERROR_SUFFIX          (_LastError > 0 ? (" [" + _err.last_message(false) + "]") : "")
#define DEBUG_MESSAGE(M)      (string(FUNC_LINE_PREFIX) + string(M) + (ERROR_SUFFIX))

#define VV(V)                 (#V + "=" + string(V))
#define VVD(V, D)             (#V + "=" + ::DoubleToString((V), (D)))
#define VVDC(V)               (#V + "=" + _double.to_string_compact(V))
#define VVE(V)                (#V + "=" + ::EnumToString(V))
#define VVPAD(V, P)           (#V + "=" + _str.pad(string(V), P))
#define VVT(V)                (#V + "=" + ::TimeToString(V))

#define VAR(V)                ("  " + VV(V))
#define VARD(V, D)            ("  " + VVD(V, D))
#define VARDC(V)              ("  " + VVDC(V))
#define VARE(V)               ("  " + VVE(V))
#define VART(V)               ("  " + VVT(V))
#define VARPAD(V, P)          ("  " + VVPAD(V, P))


#ifdef DEBUG

#define PRINT(M)              { ::Print(DEBUG_MESSAGE(M)); }
#define PRINT_RET(M)          { ::Print(DEBUG_MESSAGE(M)); return; }
#define PRINT_RETURN(M, R)    { ::Print(DEBUG_MESSAGE(M)); return(R); }
#define PRINT_BREAK(M)        { ::Print(DEBUG_MESSAGE(M)); break; }

#define LOG(M)                { _log.show(M); }

#define DEBUG_DO(A)           { A; }

#else

#define PRINT(M)              { }
#define PRINT_RET(M)          { return; }
#define PRINT_RETURN(M, R)    { return(R); }
#define PRINT_BREAK(M)        { break; }

#define LOG(M)                { }

#define DEBUG_DO(A)           { }

#endif


/* Предупреждения */

//#define DEBUG_ALERT

// Способ предупреждений по умолчанию в режиме отладки
#ifdef DEBUG
	#ifndef DEBUG_ALERT
		#ifndef DEBUG_PRINT
			#define DEBUG_PRINT
		#endif
	#endif
#endif


#include "type/uncopyable.mqh"


class CBDebug: CBUncopyable
{
public:

	void warning(string message)
	{
#ifdef DEBUG_PRINT
		::Print("WARNING: ", message);
		return;
#endif

#ifdef DEBUG_ALERT
		::Alert("WARNING: ", message);
		return;
#endif
	}

	void info(string message)
	{
#ifdef DEBUG_PRINT
		::Print("INFO: ", message);
		return;
#endif

#ifdef DEBUG_ALERT
		::Alert("INFO: ", message);
		return;
#endif
	}

	bool assert(bool cond, string msg)
	{
		if (cond)
			return(true);
			
		PRINT("ASSERTION FAILED: " + msg);
		return(false);
	}

} _debug;

// (C) должно выполняться всегда
// result
#define ASSERT(C)        _debug.assert((C), #C)
#define ASSERT_MSG(C, M) _debug.assert((C), #C + string(M))
// return
#define ASSERT_RET(C)                { if (!(C)) PRINT_RET    ("ASSERTION FAILED: " + #C); }
#define ASSERT_RETURN(C, R)          { if (!(C)) PRINT_RETURN ("ASSERTION FAILED: " + #C, (R)); }
#define ASSERT_MSG_RETURN(C, M, R)   { if (!(C)) PRINT_RETURN ("ASSERTION FAILED: " + #C + ". " + M, (R)); }
