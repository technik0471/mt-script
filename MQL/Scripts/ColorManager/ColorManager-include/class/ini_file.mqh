/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// INI-файл. Минимальная реализация. © FXcoder

/*
1. Комментарии только на всю строку, строка начинается с `;`.
2. Если переменная повторяется, то будет использовано последнее значение.
3. Параметры trim_* указывают необходимость обрезки строк, значений, названий секций и переменных.
4. В строгом режиме все строки и значения считываются как есть, иначе строки, секции, имена и значения
     будут обрезаны (убраны пробельные символы на концах)

Пример:
; comment
[Group]
variable=value

todo: уточнить взаимное влияние параметров предобработки
*/


#property strict

#include "../bsl.mqh"


class CIniFile
{
protected:

	// stored names, values, sections are always prepared (trimmed)
	string sections_names_[];
	CBDict<string, string> *sections_[];

	const bool strict_mode_; // if true, no strings will be trimmed

public:

	void CIniFile():
		strict_mode_(false)
	{
		init();
	}

	void CIniFile(bool strict_mode):
		strict_mode_(strict_mode)
	{
		init();
	}

	void ~CIniFile()
	{
		_ptr.safe_delete_array(sections_);
	}

	void init()
	{
		_ptr.safe_delete_array(sections_);
		
		_arr.resize(sections_, 0);
		_arr.resize(sections_names_, 0);
		
		add_section(""); // 0: global
	}

	bool load(string path, int flags)
	{
		init();
		
		string lines[];
		int nlines = _file.read_lines(path, flags, lines);
		
		if (nlines <= 0)
			return(false);
		
		// global section by default
		CBDict<string, string> *section = sections_[0];
		
		for (int i = 0; i < nlines; i++)
		{
			string line = prepare(lines[i]);
			int len = StringLen(line);
			
			// empty line
			if (len <= 0)
				continue;
			
			// comment
			if (line[0] == ';')
				continue;
			
			// new section
			if ((line[0] == '[') && (line[len - 1] == ']'))
			{
				string section_name = prepare(StringSubstr(line, 1, len - 2));
				section = get_section(section_name);
				continue;
			}
			
			// value
			int eq_pos = StringFind(line, "=");
			
			if (eq_pos < 0)
			{
				_debug.warning("Wrong ini file line: [" + line + "]");
				continue;
			}
			
			string name = prepare(StringSubstr(line, 0, eq_pos));
			string value = prepare(StringSubstr(line, eq_pos + 1));
			section.set(name, value);
		}
		
		return(true);
	}

	bool save(string path, int flags)
	{
		string lines[];
		get_lines(lines);
		return(_file.write_lines(path, flags, lines));
	}

	int get_lines(string &lines[])
	{
		_arr.resize(lines, 0);
		bool is_empty = true;
		
		for (int i = 0, nsections = ArraySize(sections_); i < nsections; i++)
		{
			CBDict<string, string> *section = sections_[i];

			// first section has no name
			if (i > 0)
			{
				if (!is_empty)
					_arr.add(lines, "");
				
				_arr.add(lines, "[" + sections_names_[i] + "]");
			}
			
			for (int j = 0, nvars = section.size(); j < nvars; j++)
			{
				string name = section.key(j);
				string value = section.value(j);
				_arr.add(lines, name + "=" + value);
			}
			
			is_empty = is_empty && (ArraySize(lines) == 0);
		}
		
		return(ArraySize(lines));
	}

	int get_value(string var_name, string section_name, int fallback)
	{
		string value_string;
		// it is better here to ignore strict mode for values
		return(check_get_value_string(prepare(var_name), prepare(section_name), value_string) ? (int)StringToInteger(_str.trim(value_string)) : fallback);
	}

	double get_value(string var_name, string section_name, double fallback)
	{
		string value_string;
		// it is better here to ignore strict mode for values
		return(check_get_value_string(prepare(var_name), prepare(section_name), value_string) ? StringToDouble(_str.trim(value_string)) : fallback);
	}

	color get_value(string var_name, string section_name, color fallback)
	{
		string value_string;
		// it is better here to ignore strict mode for values
		return(check_get_value_string(prepare(var_name), prepare(section_name), value_string) ? StringToColor(_str.trim(value_string)) : fallback);
	}

	string get_value(string var_name, string section_name, string fallback)
	{
		// fallback ignores trim_value_
		string value_string;
		return(check_get_value_string(prepare(var_name), prepare(section_name), value_string) ? prepare(value_string) : fallback);
	}

	template<typename T>
	void set_value(string var_name, string section_name, T value)
	{
		CBDict<string, string> *section = get_section(prepare(section_name));
		section.set(prepare(var_name), prepare((string)value));
	}

	bool section_exists(string section_name)
	{
		return(_ptr.is_valid(find_section(prepare(section_name))));
	}

	bool var_exists(string var_name, string section_name)
	{
		string value_string; // dummy
		return(check_get_value_string(prepare(var_name), prepare(section_name), value_string));
	}

	int get_sections(string &sections[])
	{
		return(_arr.clone(sections, sections_names_));
	}

	int sections_count()
	{
		return(ArraySize(sections_));
	}

	// Does nos delete global section.
	// Deletes all occurrences.
	// Returns true if smth deleted.
	bool delete_section(string section_name)
	{
		section_name = prepare(section_name);
		if (section_name == "")
			return(false);
		
		bool deleted = false;
		
		for (int i = sections_count() - 1; i >= 0; i--)
		{
			if (section_name == sections_names_[i])
			{
				_ptr.safe_delete(sections_[i]);
				
				_arr.remove(sections_, i);
				_arr.remove(sections_names_, i);
				
				deleted = true;
			}
		}
	
		return(deleted);
	}


private:

	// In the not public methods do not prepare strings.

	// NULL, if not found
	CBDict<string, string> *find_section(string section_name)
	{
		// find forward, first section (empty name) is most expected
		for (int i = 0, count = ArraySize(sections_names_); i < count; i++)
		{
			if (sections_names_[i] == section_name)
				return(sections_[i]);
		}
		
		return(NULL);
	}

	// add section if not found
	CBDict<string, string> *get_section(string section_name = "")
	{
		CBDict<string, string> *section = find_section(section_name);
		if (_ptr.is_valid(section))
			return(section);
		
		section = new CBDict<string, string>();
		_arr.add(sections_, section);
		_arr.add(sections_names_, section_name);
		return(section);
	}

	void add_section(string section_name)
	{
		get_section(section_name);
	}

	bool check_get_value_string(string var_name, string section_name, string &value)
	{
		CBDict<string, string> *section = find_section(section_name);
		if (_ptr.is_invalid(section))
			return(false);
		
		return(section.try_get_value(var_name, value));
	}

	string prepare(string s)
	{
		return(strict_mode_ ? s : _str.trim(s));
	}

};
