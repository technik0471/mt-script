/*
Copyright 2019 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

// Extended chart properties and functions (using DLL). Better Standard Library. © FXcoder
//TODO: is_visible на основе перекрытия другими окнами и состояния минимизации

#property strict

#include "chart.mqh"
#include "chartwindowx.mqh"
#include "mql.mqh"

class CBChartX: public CBChart
{
public:

	// Default constructor
	void CBChartX():
		CBChart()
	{
	}

	// Constructor for a specific chart Id.
	void CBChartX(long chart_id):
		CBChart(chart_id)
	{
	}

	// Constructor with opening the chart.
	void CBChartX(string symbol, ENUM_TIMEFRAMES timeframe):
		CBChart(symbol, timeframe)
	{
	}

	bool is_maximized(bool fallback) const
	{
#ifdef __MQL4__
	#ifdef DEBUG
		return(fallback);
	#else
		if (!_mql.dlls_allowed())
			return(fallback);
		
		CBChartWindowX w(id_);
		if (!w.is_valid())
			return(fallback);
		
		return(w.is_maximized());
	#endif
#else
		return((bool)get(CHART_IS_MAXIMIZED)); // вероятно, r/o
#endif
	}

	bool is_minimized(bool fallback) const
	{
#ifdef __MQL4__
	#ifdef DEBUG
		return(fallback);
	#else
		if (!_mql.dlls_allowed())
			return(fallback);
		
		CBChartWindowX w(id_);
		if (!w.is_valid())
			return(fallback);
		
		return(w.is_minimized());
	#endif
#else
		return((bool)get(CHART_IS_MINIMIZED)); // вероятно, r/o
#endif
	}


	CBChartX *maximize()
	{
		if (!_mql.dlls_allowed())
			return(&this);

		CBChartWindowX w(id_);
		if (w.is_valid())
			w.maximize();
		
		return(&this);
	}

	CBChartX *minimize()
	{
		if (!_mql.dlls_allowed())
			return(&this);
		
		CBChartWindowX w(id_);
		if (w.is_valid())
			w.minimize();
		
		return(&this);
	}

	CBChartX *restore()
	{
		if (!_mql.dlls_allowed())
			return(&this);
		
		CBChartWindowX w(id_);
		if (w.is_valid())
			w.restore();
		
		return(&this);
	}

	bool is_visible() const
	{
		if (is_minimized(false))
			return(false);
		
		if (is_maximized(true))
			return(true);
		
		// Если есть хотя бы один максимизированный график, кроме текущего, то текущий скрыт за ним
		long this_chart_id = id();
		
		// Переключить все, кроме текущего
		for (CBChartX chart; chart.loop_real(); )
		{
			if (chart.id() == this_chart_id)
				continue;
			
			if (chart.is_maximized(false))
				return(false);
		}
		
		//TODO: проверка на основе перекрытия окон...
		/*
		Необходимые функции:
			- окно графика перед текущим?
		*/
		
		return(true);
	}

} _chartx;
