/*
Copyright 2019 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

// Window functions. Better Standard Library. © FXcoder

//todo: разобраться с путаницей, какой класс сделать родителем. либо сделать один владельцем другого

#property strict

#include "type/uncopyable.mqh"

class CBWindow: public CBUncopyable
{
protected:

	int handle_;


public:

	void CBWindow(): handle_(0) { }
	void CBWindow(int handle): handle_(handle) { }

	int  handle() const { return(handle_); }
	void handle(int handle) { handle_ = handle; }

	bool is_valid() const { return(handle_ != 0); }

};
