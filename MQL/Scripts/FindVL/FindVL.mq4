/*
Copyright 2019 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright   "FindVL 5.1. © FXcoder"
#property link        "https://fxcoder.blogspot.com"
#property description "FindVL: Find Volume Levels"
#property strict
#property script_show_inputs

//#define DEBUG

#include "FindVL-include/bsl.mqh"
#include "FindVL-include/enum/point_scale.mqh"
#include "FindVL-include/hist/rates_cache.mqh"
#include "FindVL-include/util/stat.mqh"
#include "FindVL-include/volume/vp_calc.mqh"
#include "FindVL-include/volume/vp_tick_params.mqh"

enum ENUM_FVL_PARAM
{
	FVL_PARAM_MAX     = 0x100,  // Maximum
	FVL_PARAM_MEDIAN  = 0x200,  // Median
	FVL_PARAM_VWAP    = 0x300,  // VWAP
};

#ifndef INPUT_GROUP
#define INPUT_GROUP ""
#endif

input bool                   ClearOnly      = false;                 // Clear Only

input string                 g_rng_mode_    = INPUT_GROUP;           // •••••••••• RANGE ••••••••••
input int                    RangeBars      = 24;                    // Range Bars
input int                    Step           = 1;                     // Step

input string                 g_data_        = INPUT_GROUP;           // •••••••••• DATA ••••••••••
input ENUM_VP_SOURCE         DataSource     = VP_SOURCE_M1;          // Data Source

#ifdef __MQL4__
      ENUM_APPLIED_VOLUME    VolumeType     = VOLUME_TICK;           // Volume Type (always TICK in 4)

      ENUM_VP_TICK_PRICE     TickPriceType  = VP_TICK_PRICE_LAST;    // Price Type
      bool                   TickBid        = true;                  // Bid Price Changed
      bool                   TickAsk        = true;                  // Ask Price Changed
      bool                   TickLast       = true;                  // Last Price Changed
      bool                   TickVolume     = true;                  // Volume Changed
      bool                   TickBuy        = true;                  // Buy Deal
      bool                   TickSell       = true;                  // Sell Deal
#else
input ENUM_APPLIED_VOLUME    VolumeType     = VOLUME_TICK;           // Volume Type

input string                 g_tick_        = INPUT_GROUP;           // •••••••••• TICK ••••••••••
input ENUM_VP_TICK_PRICE     TickPriceType  = VP_TICK_PRICE_LAST;    // Price Type
input bool                   TickBid        = true;                  // Bid Price Changed
input bool                   TickAsk        = true;                  // Ask Price Changed
input bool                   TickLast       = true;                  // Last Price Changed
input bool                   TickVolume     = true;                  // Volume Changed
input bool                   TickBuy        = true;                  // Buy Deal
input bool                   TickSell       = true;                  // Sell Deal
#endif

input string                 g_calc_        = INPUT_GROUP;           // •••••••••• CALCULATION ••••••••••
input ENUM_FVL_PARAM         Parameter      = FVL_PARAM_MAX;         // Parameter
input int                    Smooth         = 0;                     // Smooth Depth (0 => disable)
input ENUM_POINT_SCALE       PointScale     = POINT_SCALE_1;         // Poing Scale

input string                 g_colors_      = INPUT_GROUP;           // •••••••••• COLORS ••••••••••
input color                  RangeColor     = clrRoyalBlue;          // Range Color
input color                  LevelColor     = clrLimeGreen;          // Level Color
input color                  NewLevelColor  = clrOrange;             // New Level Color

input string                 g_service_     = INPUT_GROUP;           // •••••••••• SERVICE ••••••••••
input string                 Id             = "fvl1";                // Id


void OnStart()
{
	string prefix = Id + " ";
	_chart.objects_delete_all(prefix);

	if (ClearOnly)
		return;

	bool show_range = _color.is_valid(RangeColor);
	bool show_level = _color.is_valid(LevelColor);
	bool show_new_level = _color.is_valid(NewLevelColor);

	if (!(show_range || show_level || show_new_level))
		return;

	CVPTickParams tick_(TickPriceType, TickBid, TickAsk, TickLast, TickVolume, TickBid, TickSell);

	double hg_point = _Point * PointScale;
	CVPCalc vpcalc_(DataSource, VolumeType, hg_point, TickPriceType, tick_.flags());
	
	// forward range to determine the need to find the intersection
	MqlRates rate;
	if (!_ratescache.get_rate(0, rate))
	{
		Print("Error: no zero bar.");
		return;
	}

	double low = rate.low;
	double high = rate.high;
	
	datetime horizon = vpcalc_.get_horizon();
	MqlRates rate_from, rate_to;
	double prev_level = 0;
	int repeat_count = 1;
	
	for (int i = 0, nbars = _series.bars_count(); i < nbars; i += Step)
	{
		if (IsStopped())
			break;
		
		// get range
		
		int i_from = i + RangeBars - 1;
		
		if (!_ratescache.get_rate(i_from, rate_from))
			break;
		
		if (!_ratescache.get_rate(i, rate_to))
			break;
		
		// range
		datetime time_from = rate_from.time;
		datetime time_to   = rate_to.time;
		
		if (time_from < horizon)
			break;

		// update forward range
		bool res = true;
		for (int j = _math.max(i - Step, 0); j < i; j++)
		{
			if (!_ratescache.get_rate(j, rate))
			{
				res = false;
				break;
			}
					
			low  = fmin(low,  rate.low);
			high = fmax(high, rate.high);
		}
		
		if (!res)
		{
			Print("Error while forward range updating.");
			break;
		}
		

		// calc hg and max
		
		double low_price;
		double volumes[];
		
#ifdef __MQL4__
			const int count = vpcalc_.get_hg(time_from, time_to, low_price, volumes);
#else
			const int count = (DataSource == VP_SOURCE_TICKS)
				? vpcalc_.get_hg_by_ticks   (time_from, time_to, low_price, volumes)
				: vpcalc_.get_hg            (time_from, time_to, low_price, volumes);
#endif
		
		if (count <= 0)
		{
			PRINT("Error:" + VAR(time_from) + VAR(time_to) + VAR(count));
			break;
		}
		
		vpcalc_.smooth_hg(Smooth, hg_point, low_price, volumes);
		
		int level_pos = -1;//
		
		switch (Parameter)
		{
			case FVL_PARAM_MAX:
				level_pos = _arr.max_index(volumes);
				break;
			
			case FVL_PARAM_MEDIAN:
				level_pos = _math.median_index(volumes);
				break;
			
			case FVL_PARAM_VWAP:
				level_pos = hg_vwap_index(volumes, low_price, hg_point);
				break;
			
			default:
				Print("Error. Unknown Parameter.");
				break;
		}
		
		// draw
		
		double level = low_price + level_pos * hg_point;
		repeat_count = (fabs(level - prev_level) < hg_point) ? repeat_count + 1 : 1;
		prev_level = level;
		
//todo: find out what is the upper limit
		int line_width = _math.limit_below(_math.round_to_int(sqrt(repeat_count)), 1);

		// draw range
		if (show_range)
			draw_trend(prefix + (string)i + " rng", time_from, level, time_to, level, RangeColor, line_width, STYLE_SOLID, true,  false);

		if ((level > high) || (level < low))
		{
			// naked/new forward
			if (show_new_level)
				draw_trend(prefix + (string)i + " nfwd", time_to,   level, time_to + _tf.current_seconds, level, NewLevelColor, line_width, STYLE_SOLID, false, true);
		}
		else
		{
			if (show_level)
			{
				// find crossing
				for (int j = i; j >= 0; j--)
				{
					if (!_ratescache.get_rate(j, rate))
						break;
					
	//todo: gaps
					if ((level <= rate.high) && (level >= rate.low))
					{
						// forward
						datetime time3 = _time.add_bars(rate.time, 1); // add a bar for good looking on lower tf
						draw_trend(prefix + (string)i + " fwd", time_to,   level, time3,   level, LevelColor, line_width, STYLE_SOLID, false, false);
						break;
					}
				}
			}
		}
		
		Comment(_mql.program_name() + ": " + (string)(i + 1));
	}

	Comment("");
}

void draw_trend(string name, datetime time1, double price1, datetime time2, double price2,
	color lineColor = Gray, int width = 1, ENUM_LINE_STYLE style = STYLE_SOLID, bool back = true, bool ray = true, int window = 0)
{
	CBGO trend(name);
	trend.recreate(OBJ_TREND, window, time1, price1, time2, price2);
	trend.colour(lineColor).style(style).width(width);
	trend.ray_left(false).ray_right(ray);
	trend.back(back).selectable(true).hidden(true);
}

/*
Last changes:

5.1:
	* setting the color to None disables the line display (at least one color must be specified)
	* when drawing a level with an intersection, additional time is added so that this intersection is visible on lower timeframes

5.0:
	* input parameter Parameter added to choose the parameter to test (maximum, median, VWAP)
	* input parameters from VP added: Smooth, DataSource
	* input parameters from VP added (MT5 only): VolumeType, TickPriceType, TickBid, TickAsk, TickLast, TickVolume, TickBuy, TickSell

4.0:
	* fixed: ray to the left of range
	* HistoryStep -> Step
*/
