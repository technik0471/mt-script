/*
Copyright 2019 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

// Масштаб пункта. © FXcoder

#property strict

enum ENUM_POINT_SCALE
{
	POINT_SCALE_1     = 1,    // *1
	POINT_SCALE_10    = 10,   // *10
	POINT_SCALE_100   = 100,  // *100
};
