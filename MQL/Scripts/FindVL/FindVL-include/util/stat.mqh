/*
Copyright 2019 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

// Статистические функции. © FXcoder
#property strict

#include "../bsl.mqh"

// Получить моды (локальные максимумы) гистограммы
int hg_modes(const double &values[], const int mode_step, int &modes[])
{
	int mode_count = 0;
	ArrayFree(modes);

	// ищем максимумы по участкам
	for (int i = mode_step, count = ArraySize(values) - mode_step; i < count; i++)
	{
		const int max_from = i - mode_step;
		const int max_range = 2 * mode_step + 1;
		const int max_to = max_from + max_range - 1;
		
		const int k = _arr.max_index(values, max_from, max_range);
		
		if (k != i)
			continue;
		
		for (int j = i - mode_step; j <= i + mode_step; j++)
		{
			if (values[j] != values[k])
				continue;
			
			mode_count++;
			ArrayResize(modes, mode_count, count);
			modes[mode_count - 1] = j;
		}
	}

	return(mode_count);
}


// Вычислить VWAP и вернуть индекс элемента.
int hg_vwap_index(const double &volumes[], double low, double step)
{
	if (step == 0)
		return(-1);

	double vwap = 0;
	double total_volume = 0;
	int size = ArraySize(volumes);

	for (int i = 0; i < size; i++)
	{
		double price = low + i * step;
		double volume = volumes[i];
		
		vwap += price * volume;
		total_volume += volume;
	}

	if (total_volume == 0)
		return(-1);

	vwap /= total_volume;
	return(_math.round_to_int((vwap - low) / step));
}


// Вычислить VWAP и вернуть индекс элемента.
int hg_vwap_index(const double &prices[], const double &volumes[])
{
	double vwap = 0;
	double total_volume = 0;
	int size = ArraySize(volumes);

	if (size != ArraySize(prices))
		return(-1);

	if (size == 0)
		return(-1);

	for (int i = 0; i < size; i++)
	{
		double price = prices[i];
		double volume = volumes[i];
		
		vwap += price * volume;
		total_volume += volume;
	}

	if (total_volume == 0)
		return(-1);

	vwap /= total_volume;


	// Найти индекс ближайшей к VWAP цены

	double min_error = MathAbs(vwap - prices[0]);
	int min_error_index = 0;

	for (int j = 1; j < size; j++)
	{
		double error = MathAbs(vwap - prices[j]);
		
		if (error < min_error)
		{
			min_error = error;
			min_error_index = j;
		}
	}

	return(min_error_index);
}


bool array_to_hg(const double &values[], int n, double &hg[])
{
	if (n < 1)
		return(false);

	int count = ArraySize(values);
	if (count <= 0)
		return(false);

	// определить границы
	double min = _math.min(values);
	double max = _math.max(values);

	return(array_to_hg(values, n, min, max, hg));
}

//TODO: int hg?
// Преобразовать массив значений в гистограмму распределения с заданным числом разбиений
bool array_to_hg(const double &values[], int n, double min, double max, double &hg[])
{
	double bins[];
	int freqs[];

	if (!array_to_hg(values, n, min, max, bins, freqs))
		return(false);

	return(_arr.clone(hg, freqs) == n);
}

bool array_to_hg(const double &values[], int n, double min, double max, double &bins[], int &freqs[])
{
	if (n < 1)
		return(false);

	int count = ArraySize(values);
	if (count <= 0)
		return(false);

	//TODO: ограничение на основе минимального шага?
	if (max == min)
		PRINT_RETURN("max == min" + VAR(max) + VAR(count), false);


	ArrayResize(bins, n);
	double step = double(n) / (max - min);

	for (int i = 0; i < n; i++)
		bins[i] = step * (0.5 + i); // для double указывать на центра бара


	ArrayResize(freqs, n);
	ArrayInitialize(freqs, 0);
	double n_per_range = n / (max - min);
	int last = n - 1;

	for (int i = 0; i < count; i++)
	{
		int pos = (int)floor((values[i] - min) * n_per_range);
		
		if (pos > last)
			pos = last;  // крайнее значение идёт в последнюю ячейку
		else if (pos < 0)
			pos = 0;     // не должно быть
		
		freqs[pos]++;
	}


	return(true);
}

