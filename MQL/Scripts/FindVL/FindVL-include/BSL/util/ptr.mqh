/*
Copyright 2019 FXcoder

This file is part of FindVL.

FindVL is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FindVL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with FindVL. If not, see
http://www.gnu.org/licenses/.
*/

// Функции указателей. Better Standard Library. © FXcoder

#property strict

#include "../type/uncopyable.mqh"

class CBPointerUtil: public CBUncopyable
{
public:

	template <typename T>
	static void safe_delete(T obj)
	{
		if (is_dynamic(obj))
			delete obj;
	}

	template <typename T>
	static void safe_delete_array(T &arr[])
	{
		for (int i = ::ArraySize(arr) - 1; i >= 0; i--)
			safe_delete(arr[i]);
	}

	template <typename T> static bool is_valid    (const T obj) { return(::CheckPointer(obj) != POINTER_INVALID  ); }
	template <typename T> static bool is_invalid  (const T obj) { return(::CheckPointer(obj) == POINTER_INVALID  ); }
	template <typename T> static bool is_automatic(const T obj) { return(::CheckPointer(obj) == POINTER_AUTOMATIC); }
	template <typename T> static bool is_dynamic  (const T obj) { return(::CheckPointer(obj) == POINTER_DYNAMIC  ); }

} _ptr;
