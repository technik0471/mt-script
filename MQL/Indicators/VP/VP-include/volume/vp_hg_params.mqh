/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// VP histogram parameters. © FXcoder

#property strict

#include "../s.mqh"
#include "enum/vp_bar_style.mqh"

class CVPHgParams
{
protected:

	ENUM_VP_BAR_STYLE      bar_style_;
	color                  color1_;
	color                  color2_;
	int                    line_width_;
	uint                   width_pct_;


public:

	_GET(ENUM_VP_BAR_STYLE,      bar_style)
	_GET(color,                  color1)
	_GET(color,                  color2)
	_GET(int,                    line_width)
	_GET(uint,                   width_pct)

	void CVPHgParams(
		ENUM_VP_BAR_STYLE      bar_style,
		color                  color1,
		color                  color2,
		int                    line_width,
		uint                   width_pct
	):
		bar_style_(bar_style),
		color1_(color1),
		color2_(color2),
		line_width_(line_width),
		width_pct_(width_pct)
	{
	}

};
