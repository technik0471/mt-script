/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// VP levels parameters. © FXcoder

#property strict

#include "../s.mqh"

class CVPLevelsParams
{
protected:

	color                  mode_color_;
	color                  max_color_;
	color                  median_color_;
	color                  vwap_color_;
	int                    mode_line_width_;
	ENUM_LINE_STYLE        stat_line_style_;
	color                  mode_level_color_;
	int                    mode_level_width_;
	ENUM_LINE_STYLE        mode_level_style_;


public:

	_GET(color,                  mode_color)
	_GET(color,                  max_color)
	_GET(color,                  median_color)
	_GET(color,                  vwap_color)
	_GET(int,                    mode_line_width)
	_GET(ENUM_LINE_STYLE,        stat_line_style)
	_GET(color,                  mode_level_color)
	_GET(int,                    mode_level_width)
	_GET(ENUM_LINE_STYLE,        mode_level_style)

	void CVPLevelsParams(
		color                  mode_color,
		color                  max_color,
		color                  median_color,
		color                  vwap_color,
		int                    mode_line_width,
		ENUM_LINE_STYLE        stat_line_style,
		color                  mode_level_color,
		int                    mode_level_width,
		ENUM_LINE_STYLE        mode_level_style
	):
		mode_color_(mode_color),
		max_color_(max_color),
		median_color_(median_color),
		vwap_color_(vwap_color),
		mode_line_width_(mode_line_width),
		stat_line_style_(stat_line_style),
		mode_level_color_(mode_level_color),
		mode_level_width_(mode_level_width),
		mode_level_style_(mode_level_style)
	{
	}

};
