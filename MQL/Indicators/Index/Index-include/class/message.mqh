/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Cообщение в окне индикатора. © FXcoder

#property strict

#include "../bsl.mqh"
//#include <+/g/glabel.mqh>


class CIndicatorMessage: public CBUncopyable
{
private:

	bool is_message_exists_;


public:

	void CIndicatorMessage():
		is_message_exists_(true) // true для принунидильного удаления в hide при первом запуске
	{
	}

	void show(const string &lines[], color text_color = clrNONE, int font_size = 7, string font_name = "Tahoma")
	{
		int subwindow = ChartWindowFind();
		ObjectsDeleteAll(0, "message -1 ", subwindow);
		
		int count = ArraySize(lines);
		int step = font_size * 3 / 2;
		
		for (int i = 0; i < count; i++)
		{
			CBGO msg("message " + (string)subwindow + " " + (string)i, OBJ_LABEL, subwindow, 0, 0);
			msg.xy(3, 3 + step * i).corner(CORNER_RIGHT_UPPER).anchor(ANCHOR_RIGHT_UPPER);
			msg.selectable(true).hidden(true).back(false); // selectable, чтобы можно было растащить наложившиеся
			msg.text(lines[i], font_size, font_name, _color.is_none(text_color) ? _chart.color_foreground() : text_color);
		}
		
		is_message_exists_ = true;
	}

	void show(string text, color text_color = clrNONE, int font_size = 7, string font_name = "Tahoma")
	{
		string lines[1];
		lines[0] = text;
		show(lines, text_color, font_size, font_name);
	}


	void show(string text1, string text2, color text_color = clrNONE, int font_size = 7, string font_name = "Tahoma")
	{
		string lines[2];
		lines[0] = text1;
		lines[1] = text2;
		show(lines, text_color, font_size, font_name);
	}

	void hide()
	{
		if (!is_message_exists_)
			return;
			
		int subwindow = ChartWindowFind();
		ObjectsDeleteAll(0, "message -1 ", subwindow);
		ObjectsDeleteAll(0, "message " + (string)subwindow + " ", subwindow);
		is_message_exists_ = false;
	}

} _imsg;

