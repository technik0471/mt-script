/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс синхронизированной истории по нескольким символам. © FXcoder

//todo: при отсутствии бара подставлять поддельный с ценой закрытия предыдущего, простое дублирование работало только для цены

#property strict

#include "../bsl.mqh"


class CSyncHistory: public CBUncopyable
{
protected:

	string            time_symbol_;  // Символ
	ENUM_TIMEFRAMES   time_period_;  // Таймфрейм
	datetimes         timeline_;     // [0..nbars_-1] даты временнОй линии, с которой синхронизируется история (0 - левый, старейший бар)
	string            symbols_[];    // [0..nsymbols_-1] символы истории
	refs<MqlRates>   *history_[];    // [0..nsymbols_-1][0..nbars_-1] синхронизированная история

	int nbars_;          // размер истории в барах
	int nsymbols_;       // количество символов
	int last_sync_bar_;  // последний синхронизированный бар (может быть не последним, если по последним барам есть данные не по всем символам). -1, если баров нет
	int max_gap_;        // максимальный зазор (рассинхронизация) в барах, 0 - идеально


public:

	int bars_count    () const { return(nbars_); }
	int symbols_count () const { return(nsymbols_); }
	int last_sync_bar () const { return(last_sync_bar_); }
	int max_gap       () const { return(max_gap_); }

	refs<MqlRates> *operator[](const int i) const { return(history_[i]); }

	datetimes *timeline ()      { return(&timeline_); }
	datetime   time     (int i) { return(timeline_.data[i]); }
	string     symbol   (int i) { return(symbols_[i]); }

	// число несинхронизированных баров (с конца)
	int unsync_bars() const { return(nbars_ - (last_sync_bar_ + 1)); }

	// Конструктор по умолчанию
	void CSyncHistory()
	{
		clear();
		time_symbol_ = _Symbol;
		time_period_ = (ENUM_TIMEFRAMES)_Period;
	}

	// Конструктор 2
	void CSyncHistory(string timeline_symbol, ENUM_TIMEFRAMES period)
	{
		clear();
		time_symbol_ = timeline_symbol;
		time_period_ = period;
	}

	void ~CSyncHistory()
	{
		_ptr.safe_delete_array(history_);
	}

	void clear()
	{
		_ptr.safe_delete_array(history_);
		ArrayFree(history_);
		ArrayFree(symbols_);
		timeline_.resize(0);

		nbars_ = 0;
		nsymbols_ = 0;
		last_sync_bar_ = -1;
		max_gap_ = 0;
	}

	// (Пере)создать временнУю линию
	bool init(datetime start_time, datetime stop_time)
	{
		clear();
		nbars_ = CopyTime(time_symbol_, time_period_, start_time, stop_time, timeline_.data);
		last_sync_bar_ = nbars_ - 1;
		return(nbars_ > 0);
	}

	// (Пере)создать временнУю линию, нумерация баров как в CopyTime
	bool init(int start_bar, int bars_count)
	{
		clear();
		nbars_ = CopyTime(time_symbol_, time_period_, start_bar, bars_count, timeline_.data);
		last_sync_bar_ = nbars_ - 1;
		return(nbars_ > 0);
	}


	// Варианты использования:
	//	1. Необходимо минимальное число баров (1 нужен всегда) и максимум желаемого (часто бесконечность или из параметра MaxBars).
	//	2. Строго необходимое число баров.
	//	3. Столько, сколько есть (вряд ли меньше 1)
	// Таким образом, доступное число баров меньше 1 приводит к результату false, т.к. этот вариант не нужен никогда.
	// Параметры:
	//   min_bars - минимальное (необходимое) число баров
	//   max_bars - максимальное (желаемое) число баров
	// Результат: количество баров истории, 0 при отсутствии необходимой истории
	int update(const string &symbols[], int min_bars, int max_bars = INT_MAX)
	{
		int min = _math.max(min_bars, 1);
		int max = _math.max(min, max_bars);
		
		// Наличие минимума истории
		int history_max_bars = get_max_bars(symbols);
		
		// Если история короче минимальной необходимой, вернуть false
		if (history_max_bars < min)
			PRINT_RETURN("history_max_bars < min: history_max_bars=" + (string)history_max_bars + " < need_bars=" + (string)min, 0);
		
		// Загружаемое число баров истории
		int bars = _math.min(history_max_bars, max);
		
		// Инициализировать историю
		if (!init(0, bars))
			PRINT_RETURN("!init, bars=" + (string)bars, 0);
		
		// Добавить в историю все символы всех формул
		if (!add_symbols(symbols))
			PRINT_RETURN("!add_symbols", 0);
		
		return(nbars_);
	}


//todo: вариант с указанием символа
	// Получить номер бара в истории по времени со смещением в прошлое при необходимости.
	// -1, если время левее истории
	int get_bar(datetime time)
	{
		int bar = ArrayBsearch(timeline_.data, time);
		
		// ArrayBsearch не может вернуть значение меньше 0, проверить нижнее значение,
		// время по найденному бару не должно быть позднее указанного
		if (timeline_.data[bar] > time)
			return(-1);
		
		return(bar);
	}

	// Добавить и синхронизировать символ
	bool add_symbol(string symbol)
	{
		if (nbars_ <= 0)
			return(false); // true?

		if (_arr.contains(symbols_, symbol))
			return(true);
		
		datetime start_time = timeline_.data[0];
		datetime stop_time = timeline_.data[nbars_ - 1];
		
		CBSeries ser(symbol, time_period_);

		// 1. Загрузить историю по границам таймлинии
		MqlRates rates[];
		int rates_count = ser.copy_rates(start_time, stop_time, rates);
		
		// Если истории в указанном промежутке нет, загрузить один последний бар (актуально, когда bars_count = 1 на новом баре)
		if (rates_count < 0)
			rates_count = ser.copy_rates(start_time, 1, rates);
		
		// Если и теперь нет истории, то значит, что истории по символу нет вообще, и синхронизация невозможна.
		if (rates_count < 0)
			PRINT_RETURN("!CopyRates #1" + VAR(symbol) + VAR(start_time) + VAR(stop_time), false);

		
		// 2. Проверить первый (старый) бар. Если в нём дата позже, чем в таймлинии, то нужно догрузить слева ещё
		//    историю так, чтобы это время было не больше.
		//    На последнем баре такое сопоставление не нужно, т.к. будущая котировка не нужна.
		if (rates[0].time > timeline_.data[0])
		{
			PRINT("!CopyRates #2.1" + VAR(symbol) + VAR(rates[0].time) + " > " + VV(start_time));
			
			// Получить время бара заведомо не правее первого в таймлинии
			if (ser.copy_rates(start_time, 1, rates) != 1)
				PRINT_RETURN("!CopyRates #2.2" + VAR(symbol), false);
				
			
//todo: rates[0].time ~ stop_time ?
			// Загрузить котировки по обновлённому диапазону
			rates_count = ser.copy_rates(rates[0].time, stop_time, rates);
			
			if (rates_count < 0)
				PRINT_RETURN("!CopyRates #2.3", false);
			
			// Вряд ли возможно, это только для отладки проверки выше.
			// Оказалось, что возможно из-за глюка в мт5 (1545, 1596), из-за которого CopyRates не может получить больше баров, чем
			// указано в настройках терминала, даже если история доступна (например, через тот же CopyRates, если взять диапазон левее).
			// Пока этот код оставлю.
			if (rates[0].time > timeline_.data[0])
				PRINT_RETURN("!CopyRates #2.4" + VAR(symbol) + VAR(rates[0].time) + " > " + VV(timeline_.data[0]) + VAR(rates_count), false);
		}
		
		
		// 3. Пройтись по таймлинии и для каждого бара найти бар в истории по символу.
		
		// Добавить символ и массив котировок по нему
		nsymbols_++;
		int si = nsymbols_ - 1;
		
		ArrayResize(symbols_, nsymbols_);
		symbols_[si] = symbol;
		
		ArrayResize(history_, nsymbols_);
		history_[si] = new refs<MqlRates>();
		refs<MqlRates> *sym_hist = history_[si];
		sym_hist.resize(nbars_);
		
		int shift = 0; // последний сдвиг (для ускорения поиска)
		int last_sync_bar = -1; // последний синхронизированный бар для данного символа
		
		for (int bar = 0; bar < nbars_; bar++)
		{
//todo: проверить необходимость
			sym_hist.data[bar].close = EMPTY_VALUE;
			
			datetime time = timeline_.data[bar];
			int rate_bar = bar + shift;
			
			if (rate_bar > rates_count - 1)
				rate_bar = rates_count - 1;
			
			datetime rate_time = rates[rate_bar].time; // начальная точка для поиска

			// Найти бар в котировках добавляемого символа, с временем не больше времени в таймлинии
			
			if (rate_time == time) // простейший случай, когда время совпадает
			{
				last_sync_bar = bar;
				sym_hist.data[bar] = rates[rate_bar];
				// обновление зазора не нужно, т.к. он либо нулевой, либо обновлён на то же значение (=|shift|) ранее
			}
			else if (rate_time > time) // время котировки больше времени таймлинии, поиск влево
			{
				last_sync_bar = bar; // в этой ветке бар всегда будет синхронизирован
				bool found = false;
				
				// Пройти влево по котировкам, пока время не станет равно или меньше времени таймлинии
				for (int i = rate_bar - 1; i >= 0; i--)
				{
//todo: array out of range, как это возможно?
					if (rates[i].time <= time)
					{
						sym_hist.data[bar] = rates[i];
						shift = i - bar;
						found = true;
						update_max_gap(bar, i);
						break;
					}
				}
				
				
//todo: убрать после отладки
				// Учитывая то, как были предварительно загружены данные, котировка всегда найдётся,
				// оставлено для отладки, т.к. МТ не перестаёт удивлять своими багами.
				if (!found)
					PRINT_RETURN("!found, rate_time > time", false);
			}
			else // if (rate_time < time) // время котировки меньше времени таймлинии, поиск вправо
			{
				// Пройти вправо по котировкам, пока время не станет больше времени таймлинии, взять бар левее.
				bool found = false;
				
				for (int i = rate_bar + 1; i < rates_count; i++)
				{
					if (rates[i].time > time)
					{
						found = true;
						rate_bar = i - 1;
						//shift = rate_bar - bar; // если найдено точное совпадение, запомнить сдвиг
						last_sync_bar = bar; // обновлять только в случае успешно найденного бара
						break;
					}
					
					// сдвинуться вправо для получения последней котировки в случае, если не найдётся точная
					rate_bar = i;
				}

				sym_hist.data[bar] = rates[rate_bar];
				shift = rate_bar - bar; // запомнить сдвиг
				update_max_gap(bar, rate_bar);
			}
		}

		// Если последний синхронизированный бар не совпадает с конечным, заполнить оставшиеся бары последним значением.
		if (last_sync_bar != nbars_ - 1)
		{
			// Если явно синхронизированных баров нет, использовать самое первое значение для всех
			int last_value_bar = (last_sync_bar < 0) ? 0 : last_sync_bar;
			MqlRates last_rate = sym_hist.data[last_value_bar];
			
			for (int i = last_value_bar + 1; i < nbars_; i++)
				sym_hist.data[i] = last_rate;
				
			update_max_gap(nbars_ - 1, last_sync_bar);
		}
		
		// Обновить последний синхронизирванный, если он был больше текущего.
		
		if (last_sync_bar < last_sync_bar_)
			last_sync_bar_ = last_sync_bar;
		
		return(true);
	}


	// Время последнего синхронизированного бара (для которого есть данные по всем символам)
	// 0, если нет таких баров
	datetime last_sync_bar_time() const
	{
		return(last_sync_bar_ >= 0 ? timeline_.data[last_sync_bar_] : 0);
	}


	// Добавить несколько символов в историю и синхронизировать
	bool add_symbols(const string &symbols[])
	{
		for (int s = 0, count = ArraySize(symbols); s < count; s++)
		{
			if (!add_symbol(symbols[s]))
				return(false);
		}

		return(true);
	}

	// вернуть false в случае ошибки
	bool get_symbol_start_time(string symbol, datetime &start_time) const
	{
		// start_time по мнению SERIES_FIRSTDATE, может быть неверным (см. далее)
		if (!SeriesInfoInteger(symbol, time_period_, SERIES_FIRSTDATE, start_time))
			PRINT_RETURN("!SERIES_FIRSTDATE, " + symbol, false);

		// MT5 (1545, 1596) не может отдавать больше этого количества баров функцией CopyRates, даже если они доступны
		int max_bars = _terminal.max_bars();
		int bars = Bars(symbol, time_period_, start_time, TimeCurrent() + 1);
		
		// Если количество баров до найденного времени меньше указанного в терминале, то, вероятно (уже ни в чём не уверен),
		// SERIES_FIRSTDATE выдал правильную дату, можно успешно выйти.
		if (bars <= max_bars)
			return(true);
		
		// Иначе найти реальную дату, которую можно получить с помощью CopyRates от последнего бара
		
		datetime time[];
		//if (CopyTime(symbol, time_period_, 0, max_bars, time) <= 0)
		if (CopyTime(symbol, time_period_, max_bars - 1, 1, time) <= 0)
			PRINT_RETURN("!CopyTimes, " + symbol, false);
			
		datetime alt_start_time = time[0];
		
		// Если альтернативное время больше ранее найденного, то нужно использовать его, т.к. только до него доступны котировки.
		// Иного варианта здесь быть не может, т.к. в этом случае вышли бы ранее, на всяй случай (бывает!) вывести сообщение отладки.
		if (alt_start_time > start_time)
			start_time = alt_start_time;
		else
			PRINT("alt_start_time <= start_time");
			
		return(true);
	}

//todo: есть подозрение, что эта функция ест память, даже если необходимое число баров мало (задавать максимум или левую дату в параметрах?)
	// Получить число баров истории, доступной в терминале доступности данных по указанным
	// символам (в том числе для символа таймлинии с учётом). Вернуть -1 в случае ошибки.
	int get_max_bars(const string &symbols[]) const
	{
		datetime start_time;
		datetime max_start_time = 0;
		
		// Перебрать все символы, определить худшее начальное время котировок
		for (int s = 0, count = ArraySize(symbols); s < count; s++)
		{
			if (!get_symbol_start_time(symbols[s], start_time))
				PRINT_RETURN("!get_symbol_start_time, " + symbols[s], -1);
				
			if (start_time > max_start_time)
				max_start_time = start_time;
		}

		// ...учесть также символ временной линии
		if (!get_symbol_start_time(time_symbol_, start_time))
			PRINT_RETURN("!SERIES_FIRSTDATE(time_symbol_), " + time_symbol_, -1);
		
		if (start_time > max_start_time)
			max_start_time = start_time;

		int max_bars = Bars(time_symbol_, time_period_, max_start_time, TimeCurrent() + 1);
		return(max_bars);
	}


	int symbol_index(string symbol) const
	{
		return(_arr.index_of(symbols_, symbol));
	}

	refs<MqlRates> *symbol_history(string symbol) const
	{
		int sym_pos = symbol_index(symbol);
		if (sym_pos < 0)
			return(NULL);
		
		return(history_[sym_pos]);
	}


protected:

	void update_max_gap(int timeline_bar, int rates_bar)
	{
		int gap = fabs(timeline_bar - rates_bar);
		
		if (gap > max_gap_)
			max_gap_ = gap;
	}

};
