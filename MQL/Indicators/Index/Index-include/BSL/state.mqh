/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

/*
Состояние. Better Standard Library. © FXcoder

Отдельные варианты для простых типов и структур для удобства передачи параметров.

Отдельный метод с возвратом признака смены (changed) для того, чтобы разгрузить
простейший вариант (change), т.к. скорость в подобных классах часто критична.
*/

#property strict

#include "type/uncopyable.mqh"

// для простых типов
template <typename T>
class CBState: public CBUncopyable
{
protected:

	T state_;


public:

	void CBState(T init_state):
		state_(init_state)
	{
	}

	T state() const { return(state_); }

	// true, если новое состояние меняет старое (состояние меняется на новое)
	// эквивалентно changing + change
	bool changed(T state)
	{
		if (state_ == state) // !changing(state)
			return(false);
		
		state_ = state; // change(state);
		return(true);
	}

	// изменить состояние на заданное
	void change(T state)
	{
		state_ = state;
	}

	// true, если новое состояние сменит старое (состояние не меняется)
	bool changing(T state)
	{
		return(state_ != state);
	}
};


// для структур и строк (если нужна макс. скорость)
template <typename T>
class CBStateRef
{
protected:

	T state_;


public:

	void CBStateRef(const T &init_state)
	{
		state_ = init_state;
	}

	T state() const { return(state_); }

	// true, если новое состояние меняет старое (состояние меняется на новое)
	// эквивалентно changing + change
	bool changed(const T &state)
	{
		if (state_ == state) // !changing(state)
			return(false);
		
		state_ = state; // change(state);
		return(true);
	}

	// изменить состояние на заданное
	void change(const T &state)
	{
		state_ = state;
	}

	// true, если новое состояние сменит старое (состояние не меняется)
	bool changing(const T &state)
	{
		return(state_ != state);
	}

};
