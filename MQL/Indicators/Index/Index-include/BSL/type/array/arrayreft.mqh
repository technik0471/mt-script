/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс массива указателей (для структур), шаблонный. Better Standard Library. © FXcoder

#property strict

#include "../../util/arr.mqh"
#include "arraya.mqh"


template <typename T>
class CBArrayRefT: public CBArrayA
{
public:

	T data[];


public:

	void CBArrayRefT():                      CBArrayA() { }
	void CBArrayRefT(int size):              CBArrayA() { resize(size);          }
	void CBArrayRefT(int size, int reserve): CBArrayA() { resize(size, reserve); }

	//TODO: 5.1915, 2018-10-24: Stack overflow при обращении, через .data работает, тип - struct IndexFormula
	T operator[](int i) const { return(data[i]); } // не использовать, если важна скорость

	/* Implementation */

	virtual int size()                        override const { return(::ArraySize(data)); }
	virtual int resize(int size)              override       { return(::ArrayResize(data, size, reserve_)); };
	virtual int resize(int size, int reserve) override       { reserve_ = reserve; return(resize(size)); }

	int add(T &value)
	{
		return(_arr.add_ref(data, value, reserve_));
	}

	bool add(const T &values[])
	{
		int add_size = ::ArraySize(values);
		int old_size = size();
		int new_size = old_size + add_size;
		
		if (resize(new_size) != new_size)
		{
			resize(old_size); // just a try, but it looks like there is no problem to reduce an array
			return(false);
		}

		int add_start = old_size;
		
		for (int i = 0; i < add_size; i++)
			data[add_start + i] = values[i]; // override the operator `=` if it is not a simple struct
			
		return(true);
	}

	void remove(int index)
	{
		int size = size();
		if ((size < 1) || (index > size - 1) || (index < 0))
			return;
		
		// левая часть остаётся без изменений

		// правая часть
		for (int i = index + 1; i < size; i++)
			data[i - 1] = data[i];
		
		// size() не использовать, т.к. там может появиться удаление старых объектов, что здесь не нужно
		::ArrayResize(data, size - 1, reserve_);
	}

	template <typename TComparer>
	void sort(const TComparer comparer)
	{
		_arr.sort(data, comparer);
	}

	// поиск для типов, которые можно передать по значению
	template <typename TValue, typename TEqComparer>
	int index_of(TValue value, const TEqComparer eq_comparer, int starting_from = 0)
	{
		return(_arr.index_of(data, value, eq_comparer, starting_from));
	}

};
