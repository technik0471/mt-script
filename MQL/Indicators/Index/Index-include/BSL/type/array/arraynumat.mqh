/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс массива для числовых типов, абстрактная заготовка. Better Standard Library. © FXcoder

/*
Некоторые методы могли бы не быть абстрактными, но из-за бага в редакторе нет удобной
подсказки при использовании методов шаблонного класса, поэтому приходится их объявлять
явно в типизированных наследниках.
*/

#property strict

#include "arraya.mqh"


// abstract
template <typename T>
class CBArrayNumAT: public CBArrayA
{
public:

	void CBArrayNumAT(): CBArrayA() { }

	// abstract
	virtual T      operator[] (int i) const = NULL;
	virtual void   fill       (T value) = NULL;
	virtual void   fill       (T value, int first, int count) = NULL;
	virtual int    copy       (const T &src[]) = NULL;
	virtual int    copy       (const T &src[], int dst_start, int src_start, int count) = NULL; //todo: проверить использования, указывать явно count
	virtual void   clone      (const T &src[]) = NULL;
	virtual int    add        (T value) = NULL;
	virtual int    index_of   (T value, int starting_from = 0) const = NULL;
	virtual int    max_index  () const = NULL;
	virtual int    max_index  (int first, int count) const = NULL;
	virtual int    min_index  () const = NULL;
	virtual int    min_index  (int first, int count) const = NULL;
	virtual T      max  () const = NULL;
	virtual T      max  (int first, int count) const = NULL;
	virtual T      min  () const = NULL;
	virtual T      min  (int first, int count) const = NULL;
	virtual string to_string  (string separator) const = NULL;

};
