/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Элемент формулы индекса. © FXcoder

#property strict

#include "../bsl.mqh"
#include "../s.mqh"
#include "context.mqh"


class CIndexFormulaElement: public CBUncopyable
{
private:

	string symbol_; // символ, индексы в upper case
	double factor_; // множитель
	
	bool is_valid_;     // проверенный символ (достпен на рынке, либо индекс)
	bool is_pair_;      // валютная пара //todo: -> is_forex_pair
	bool is_index_;     // индекс
	bool has_prefix_;   // индекс с префиксом `!`
	CBSymbol *av_sym_;  // указатель на символ из списка доступных (только если is_available_==true)

	const CIndexContext *context_;


public:

	void CIndexFormulaElement(string symbol, double factor, const CIndexContext* &context):
		is_valid_(false),
		symbol_(_str.upper(symbol)),
		factor_(factor),
		is_pair_(false),
		is_index_(false),
		has_prefix_(false),
		av_sym_(NULL),
		context_(context)
	{
		init(symbol, factor);
	}

	_GET(string, symbol)
	_GET(double, factor)
	_GET(bool, is_valid)
	_GET(bool, is_pair)
	_GET(bool, is_index)
	_GET(bool, has_prefix)
	_GET(CBSymbol*, av_sym)

	bool is_available() const { return(_ptr.is_valid(av_sym_)); }
	
	bool init(string symbol, double factor)
	{
		is_valid_ = parse_symbol();
		return(is_valid_);
	}

	void multiply_by(double factor)
	{
		factor_ *= factor;
	}

	void plus(double factor)
	{
		factor_ += factor;
	}

	// trim the prefix `!`
	string index_name() const
	{
		return(has_prefix_ ? StringSubstr(symbol_, 1) : symbol_);
	}

	// если не найдено, вернуть без изменений
	string symbol_original() const
	{
		for (int i = context_.av_syms.size() - 1; i >= 0; i--)
		{
			string original_name = context_.av_syms[i].name();
			
			if (symbol_ == _str.upper(original_name))
				return(original_name);
		}

		return(symbol_);
	}


private:

	// Определить тип элемента формулы и проверить его корректность.
	// Распознаются как обычные символы:
	//	- имя с суффиксом и префиксом, если есть
	//	- имя валютной пары без суффикса и префикса
	// Распознаются как индексы:
	//  - USD
	//  - имя символа в списке псевдонимов
	//	- имя валюты (3 символа и наличие кросса с USD): USD, EUR
	//	- имя с ! перед ним + тест на наличие самого символа, либо кросса с USD: !USD, !EUR, !MMM, !GOLD, !SP500
	// true on valid symbol
	bool parse_symbol()
	{
		is_pair_ = false;
		is_index_ = false;
		av_sym_ = NULL;
	
		has_prefix_ = StringGetCharacter(symbol_, 0) == '!';
		string symbol = index_name();
		
		// `USD` - всегда индекс USD
		if (symbol == "USD")
		{
			is_index_ = true;
			return(true);
		}
		
		// сначала найти в псевдонимах индексов
		if (context_.index_alias_to_symbol(symbol) != "")
		{
			is_index_ = true;
			return(true);
		}
		
		// искать другими способами, сверяясь со списком доступных символов
		for (int i = context_.av_syms.size() - 1; i >= 0; i--)
		{
			CBSymbol *sym = context_.av_syms[i];
			string sym_name_upper = _str.upper(sym.name());

			// полное совпадение
			if (!has_prefix_ && (symbol == sym.name()))
			{
				is_pair_ = sym.is_forex();
				av_sym_ = sym;
				return(true);
			}

			// проверка на валютную пару и индекс валюты
			if (sym.is_forex())
			{
				string av_curr1 = _str.upper(sym.currency1());
				string av_curr2 = _str.upper(sym.currency2());
				
				// валютная пара: EURJPY, GBPUSD
				if (!has_prefix_ && ((symbol == av_curr1 + av_curr2) || (symbol == av_curr2 + av_curr1)))
				{
					is_pair_ = true;
					av_sym_ = sym;
					return(true);
				}
				
				// индекс по наличию мажора: EUR, JPY, !CAD
				if (((av_curr1 == symbol) && (av_curr2 == "USD")) ||
					((av_curr2 == symbol) && (av_curr1 == "USD")))
				{
					is_index_ = true;
					return(true);
				}
			}

			// далее не символ, проверка только на индекс
			
			// начинается с ! и символ существует и не форекс: !SP500, !WTI
			if (has_prefix_ && !sym.is_forex() && (sym_name_upper == symbol))
			{
				is_index_ = true;
				return(true);
			}
			
			// проверка (неточная) на наличие пары с USD: XAU, BTC, !ETH
			if (_str.contains(sym_name_upper, symbol + "USD"))
			{
				is_index_ = true;
				return(true);
			}
		}
		
		return(false);
	}
};
