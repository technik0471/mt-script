/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Контекст индекса (параметры и окружение). © FXcoder

#property strict

#include "../bsl.mqh"


class CIndexContext: public CBUncopyable
{
public:

	string gv_formula_prefix;
	string init_calc_currs[]; // upper case
	CBArrayPtrT<CBSymbol> *av_syms;
	CBDict<string, CBSymbol*> *aliases; // upper case

	void CIndexContext(string gv_prefix, string calc_currs, const string &index_aliases[])
	{
		this.gv_formula_prefix = gv_prefix;
		_str.split_input_csv(_str.upper(calc_currs), true, this.init_calc_currs);

		// Получить доступные символы
		
		this.av_syms = new CBArrayPtrT<CBSymbol>();
		int nsymbols = SymbolsTotal(false);

		for (int i = 0; i < nsymbols; i++)
		{
		    CBSymbol *sym = new CBSymbol(SymbolName(i, false));
			this.av_syms.add(sym);
		}
		
		
		// Разобрать псевдонимы
		
		this.aliases = new CBDict<string, CBSymbol*>();

		// должен быть прямой порядок
		for (int i = 0, a_count = ArraySize(index_aliases); i < a_count; i++)
		{
			string index;
			string usd_pairs[];
			
			if (!parse_index_alias(index_aliases[i], index, usd_pairs))
			{
				_debug.warning("!parse_index_alias: " + index_aliases[i]);
				continue;
			}
			
			CBSymbol *alias_usd_pair;

			// пропустить, если уже есть
			if (this.aliases.try_get_value(index, alias_usd_pair))
				continue;

			alias_usd_pair = NULL;

			// найти первую доступную пару
			// должен быть прямой порядок
			for (int j = 0, p_count = ArraySize(usd_pairs); j < p_count; j++)
			{
				for (int k = this.av_syms.size() - 1; k >= 0; k--)
				{
					if (usd_pairs[j] == _str.upper(this.av_syms[k].name()))
					{
						alias_usd_pair = this.av_syms[k];
						break;
					}
				}
				
				if (_ptr.is_valid(alias_usd_pair))
				{
					this.aliases.set(index, alias_usd_pair);
					break;
				}
			}
		}
		
		// найти псевдонимы в начальном наборе валют
		for (int i = ArraySize(this.init_calc_currs) - 1; i >= 0; i--)
		{
			string index_alias = symbol_to_index_alias(this.init_calc_currs[i]);
			if (index_alias != "")
				this.init_calc_currs[i] = index_alias;
		}

	}

	void ~CIndexContext()
	{
		_ptr.safe_delete_array(this.av_syms.data);
		_ptr.safe_delete(this.av_syms);
		
		for (int i = this.aliases.size() - 1; i >= 0; i--)
			_ptr.safe_delete(this.aliases.value(i));
		
		_ptr.safe_delete(this.aliases);
	}

	string index_alias_to_symbol(string index) const
	{
		CBSymbol *sym;
		return(this.aliases.try_get_value(index, sym) ? _str.upper(sym.name()) : "");
	}

	string symbol_to_index_alias(string symbol) const
	{
		symbol = _str.upper(symbol); //todo: проверить необходимость
	
		for (int i = 0, a_count = this.aliases.size(); i < a_count; i++)
		{
			if (_str.upper(this.aliases.value(i).name()) == symbol)
				return(this.aliases.key(i));
		}
		
		return("");
	}

private:

	bool parse_index_alias(string alias_string, string &index, string &usd_pairs[])
	{
		string parts[];
		
		if (_str.split(_str.upper(alias_string), ":", true, false, parts) != 2)
			return(false);
		
		index = parts[0];
		
		// parse right part with usd pairs
		int npairs = _str.split_input_csv(parts[1], true, usd_pairs);
		
		for (int i = 0; i < npairs; i++)
			usd_pairs[i] = _str.trim(usd_pairs[i]); //todo: upper?
		
		return(true);
	}
	
};
