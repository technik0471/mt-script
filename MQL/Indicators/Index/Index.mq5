/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright    "Index 10.0.5. © FXcoder"
#property link         "https://fxcoder.blogspot.com"
#property strict

#property indicator_separate_window
#property indicator_buffers 1
#property indicator_plots   1

#property indicator_label1  "Index"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

//todo: приём формул как часть своей формулы

//#define DEBUG
#define GV_FORMULA_PREFIX "+.index.formula."
#define MAX_BARS 0


#include "Index-include/bsl.mqh"
#include "Index-include/class/message.mqh"
#include "Index-include/hist/synchistory.mqh"
#include "Index-include/plot/lineplot.mqh"
#include "Index-include/index/context.mqh"
#include "Index-include/index/index.mqh"
#include "Index-include/util/indicator.mqh"


/* INPUT */

input string  Formula        = "USD";                                // Formula (±k1*sym1±k2*sym2...)
input string  IndexCalcCurrs = "AUD,CAD,CHF,EUR,GBP,JPY,NZD,USD";    // Currency Set


/* GLOBALS */

// INDEX ALIASES
// ПСЕВДОНИМЫ ИНДЕКСОВ
//
// Формат псевдонимов: "Индекс: USD пары"
// Поиск идёт до первого совпадения. Без учёта регистра.
// Варианты типа XXXUSD можно не добавлять, кроме случаев, когда необходимо установить явный приоритет.
// Псевдонимы имеют приоритет над остальными методами определения символов.
// Если необходимы разные наборы для разных брокеров, заполните значения в зависимости, например, от
// ACCOUNT_COMPANY в OnInit перед другим кодом.
string index_aliases_[] =
{
	"XAU: GOLD",
	"XAG: SILVER",
	"SP500: SP500, _SP500, SPX500, .US500Cash",
	"BTC: BTCUSD, BTCUSD.m", // BTCUSD здесь не обязателен, но даёт приоритет над BTCUSD.m
};

CLinePlot plot_(0, 0);            // линия

int              event_n_ = -1;             // номер события для получения формулы (-1 = отключено)
CBState<string>  formula_state_("");
CIndex          *index_;
datetime         last_bar_open_time_ = 0;  // время (открытия) последнего бара

// messages
string MSG_LOAD_HISTORY = _mql.program_name() + ": Waiting for history...";
string MSG_FORMULA_ERR  = _mql.program_name() + ": Error in formula";
string MSG_CALC_ERR     = _mql.program_name() + ": Cannot calculate";


/* EVENTS */

void OnInit()
{
	index_ = new CIndex(GV_FORMULA_PREFIX, IndexCalcCurrs, index_aliases_);

	plot_.draw_begin(0);
	
	_indicator.short_name(_mql.program_name() + ":" + (event_n_ >= 0 ? ("event #" + (string)event_n_) : header_formula(Formula)));
	_indicator.digits(_Digits);
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	if (!check_initialized())
	{
		_imsg.show(MSG_FORMULA_ERR, clrRed);
		return(0);
	}
	else
	{
		_imsg.hide();
	}

	last_bar_open_time_ = _time.begin_of_bar(TimeCurrent());

	int calculated_bars = update_chart(rates_total, prev_calculated);
	return(calculated_bars);
}

void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
{
	static CBState<bool> new_formula(false);
	
	_chartevent.init(id, lparam, dparam, sparam);
	
	if (_chartevent.is_custom_event(event_n_))
		new_formula.change(true);

	// OnCalculate должен обрабатываться первым, глюк в 4 (4.1090): буферы линий растягиваются только перед OnCalculate
	if (last_bar_open_time_ != _time.begin_of_bar(TimeCurrent()))
	{
		_debug.warning("last_bar_open_time_ != _time.begin_of_bar()");
		return;
	}


	// Обновить всё при получении формулы извне
	if (new_formula.state())
	{
		new_formula.change(false);
		
		if (!check_initialized(true))
			return;
		
		if (formula_state_.changed(index_.formula().to_string()))
		{
			update_chart(_series.bars_count(), 0);
			ChartRedraw(); //todo: _chart.redraw5()?
		}
	}
}

void OnDeinit(const int reason)
{
	_imsg.hide();
	_ptr.safe_delete(index_);
}


/* WORKERS */

void update_digits()
{
	double max = -DBL_MAX;
	double min = DBL_MAX;

	for (int i = 0, count = plot_.size(); i < count; i++)
	{
		double value = plot_.buffer[count - 1 - i];
		
		if (value == EMPTY_VALUE)
			break;
			
		if (value > max)
			max = value;
			
		if (value < min)
			min = value;
	}

	if (max > min)
	{
		static const int max_digits = 8;
		static const int min_digits = 3;
		static int prev_digits = max_digits;

		int digits = int(ceil(log10(1.0 / (max - min)))) + 2;
		digits = _math.limit(digits, min_digits, max_digits);

		if (prev_digits != digits)
		{
			_indicator.digits(digits);
			prev_digits = digits;
		}
	}
}

bool check_initialized(bool force = false)
{
	bool res = index_.is_initialized();
	
	if (force || !res)
		res = index_.init_formula(Formula);
	
	return(res);
}

int update_chart(int rates_total, int prev_calculated)
{
	CSyncHistory hist;

	string symbols[];
	int nsymbols = index_.get_symbols(symbols);

	int uncalc_bars = rates_total - prev_calculated + 1;
	
	if ((MAX_BARS > 0) && (uncalc_bars > MAX_BARS))
		uncalc_bars = MAX_BARS;
	
	int hist_min_bars = prev_calculated == 0 ? 1 : uncalc_bars;
	int hist_max_bars = uncalc_bars;
	int history_bars = hist.update(symbols, hist_min_bars, hist_max_bars);
	//Comment("history_bars: " + (string)history_bars +  " / unsync: " + (string)hist.UnsyncBars());

	if (history_bars <= 0)
	{
		_imsg.show(MSG_LOAD_HISTORY, clrOrange);
		_debug.warning("history_bars <= 0, history_bars=" + string(history_bars));
		return(prev_calculated);
	}

	// Скорректировать количество баров для расчёта с учётом доп. условий
	int bars_to_calc = get_bars_to_calculate(rates_total, _Symbol, prev_calculated, history_bars, 5, 0);

	// Очистить все обновляемые бары в начале работы
	plot_.empty(prev_calculated == 0 ? -1 : bars_to_calc);

	// Рассчитать значения индекса
	int hist_start = history_bars - bars_to_calc;

	if (!index_.calc(hist, hist_start, bars_to_calc, plot_.size() - bars_to_calc, plot_.buffer))
	{
		_imsg.show(MSG_CALC_ERR, clrOrange);
		_debug.warning("!index_.calc()");
		return(prev_calculated);
	}

	// Обновить начало рисования при полном обновлении
	if (prev_calculated == 0)
		plot_.draw_begin(rates_total - hist.bars_count());

	_imsg.hide();

	if (prev_calculated == 0)
		update_digits();

	// При следующем расчёте учесть число несинхронизированных баров
	return(rates_total - hist.unsync_bars());
}

string header_formula(string formula_string)
{
	return(index_.replace_patterns(_str.upper(formula_string)));
}

/*
Последние изменения

10.0:
	* исправлено: паттерны [1] и [2] работали неверно для нефорексных символов
	* поддержка двух вариантов формул: с логарифмированием и без него
	* автоматическое определение числа знаков
	* псевдонимы-исключения для индексов (в коде, см. index_aliases_)

9.0:
	* исправлено: спецсимволы не обрабатывались как формулы (например, нельзя было посмотреть индекс NG через формулу !NG)
	* валюты кроссов теперь учитываются при расчёте индексов (например, AUDUSD*USD без AUD в CalcCurrs будет давать другой результат)
	* все элементы формулы теперь рассчитываются через кроссы с USD (существенно быстрее для формул с более чем одним элементом)
	* необходимые для расчёта инструменты теперь не попадают автоматически в обзор рынка
	* убраны параметры для усреднения и вычитания средней, выполняющих роль индикатора MACD поверх Index: MAPeriod, DiffMAPeriod, MAMethod
	* убран параметр AppliedPrice, типы цен кроме цены закрытия либо неинтересны (Open), либо приводят к искажениям
	* убран параметр LogScale, формула теперь всегда считается с логарифмическим масштабом
	* изменён формат формул, возможность использовать множители для отдельных элементов
	* параметры Power и Scale заменены одним фиксированным масштабом 1.0, при необходимости множитель можно добавить к каждому элементу формулы
	* значения после логарифмирования теперь не умножаются на 100
*/
