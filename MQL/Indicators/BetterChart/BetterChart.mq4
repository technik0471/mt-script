/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "BetterChart 1.1. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict
#property indicator_chart_window
#property indicator_plots 0


#include "BetterChart-include/bsl/all.mqh"
#include "BetterChart-include/i/smartautoscroll.mqh"
#include "BetterChart-include/enum/mtkey.mqh"


input bool       SmartAutoscroll       = true;               // Enable Smart Autoscroll

#ifdef __MQL4__
input bool       AlternativeZoomInKey  = true;               // Enable Alternative Zoom In Key (=)
#else
      bool       AlternativeZoomInKey  = false;
#endif

input ENUM_MTKEY OneClickTradingKey    = MTKEY_GRAVE_ACCENT; // One Click Trading Panel Key (None = disable)


// modules
CSmartAutoscroll smartautoscroll_(0);


void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
{
	if (!_tf.is_enabled())
		return;

	_chartevent.update(id, lparam, dparam, sparam);
	
	if (SmartAutoscroll)
		smartautoscroll_.chart_event();
	
	if (_chartevent.is_key_down_event())
	{
		const ushort key = _chartevent.key();
		
		if (key == OneClickTradingKey)
		{
			// 0 key is possible (e.g. sent via WinAPI)
			if (OneClickTradingKey != MTKEY_NONE)
				on_oct_panel_key();
		}
		else if (key == MTKEY_EQUAL)
		{
#ifdef __MQL4__
			if (AlternativeZoomInKey)
				on_alt_zoom_in_key();
#endif
		}
	}
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	_tf.enable();
	return(rates_total);
}

void on_alt_zoom_in_key()
{
	int scale = _chart.scale();
	
	// 5 is the max scale
	if (scale < 5)
	{
		// See fxcoder/mki#42
		
		int last_bar = _chart.last_visible_bar();
		_chart.scale(scale + 1);
		
		// The only way to get the chart's shift is to move it to the rightmost position and get the last visible bar.
		// This method is different from the original, but it is fairly close. I don’t know how MT really works here.
		_chart.navigate_end(0);
		_chart.navigate_end(_chart.last_visible_bar() - last_bar);
		
		_chart.redraw();
	}
}

void on_oct_panel_key()
{
	_chart.show_one_click(!_chart.show_one_click());
	_chart.redraw();
}


/*
Changes:

1.1:
	* кнопка для показа/скрытия панели торговли одним кликом, #51
	* кнопка = для увеличения масштаба в MT4 (в 5 это работает стандартно), #52
*/
