/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Chart. BSL+E. © FXcoder

#property strict

#include "../s.mqh"


class CChart
{
protected:

	// could be negative (for loop functions), zero (current chart) or reak ID
	long id_; // 0 - current chart (another code depends on this, do not change the behavior)


public:

	// Default constructor
	void CChart():
		id_(0)
	{
	}

	// Constructor for a specific chart Id.
	void CChart(long chart_id):
		id_(chart_id)
	{
	}

	void redraw()
	{
		::ChartRedraw(id_);
	}

	bool navigate_end(int bars_to_navigate = 0) const
	{
		return(::ChartNavigate(id_, CHART_END, bars_to_navigate));
	}
	
	/* Standard properties */

	// Mode of automatic moving to the right border of the chart
	_E_PROP_GET(bool, autoscroll, CHART_AUTOSCROLL)
	_E_PROP_SET(bool, autoscroll, CHART_AUTOSCROLL)

	// Number of the first visible bar in the chart. Indexing of bars is the same as for timeseries (r/o)
	_E_PROP_GET(int, first_visible_bar, CHART_FIRST_VISIBLE_BAR)

	// Scale (0..5)
	_E_PROP_GET(int, scale, CHART_SCALE)
	_E_PROP_SET(int, scale, CHART_SCALE)

	// Showing the "One click trading" panel on a chart
	_E_PROP_GET(bool, show_one_click, CHART_SHOW_ONE_CLICK)
	_E_PROP_SET(bool, show_one_click, CHART_SHOW_ONE_CLICK)

	// Chart width in bars (r/o)
	_E_PROP_GET(int, width_in_bars, CHART_WIDTH_IN_BARS)


	/* Usefull functions */

	// Number of the last (rightmost) visible bar
	int last_visible_bar() const
	{
		return(first_visible_bar() - width_in_bars());
	}


protected:

	bool set(ENUM_CHART_PROPERTY_DOUBLE  property_id, double value) { return(::ChartSetDouble( id_, property_id, value)); }
	bool set(ENUM_CHART_PROPERTY_INTEGER property_id, long   value) { return(::ChartSetInteger(id_, property_id, value)); }
	bool set(ENUM_CHART_PROPERTY_STRING  property_id, string value) { return(::ChartSetString( id_, property_id, value)); }
	bool set(ENUM_CHART_PROPERTY_INTEGER property_id, int window, long value) { return(::ChartSetInteger(id_, property_id, value)); }

	double get(ENUM_CHART_PROPERTY_DOUBLE  property_id, int window = 0) const { return(::ChartGetDouble( id_, property_id, window)); }
	long   get(ENUM_CHART_PROPERTY_INTEGER property_id, int window = 0) const { return(::ChartGetInteger(id_, property_id, window)); }
	string get(ENUM_CHART_PROPERTY_STRING  property_id                ) const { return(::ChartGetString( id_, property_id        )); }

} _chart;
