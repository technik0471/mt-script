/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Timeframe. BSL+E. © FXcoder

#property strict

class CTimeframeUtil
{
private:

#ifdef __MQL4__
	static bool is_enabled_; // fxcoder/mki#31
#endif


public:

	// fxcoder/mki#31
	// hack for 4's bug: disabling of showing indicator on period affects only OnCalculate
	// should be called in OnCalculate
	static void enable()
	{
#ifdef __MQL4__
		is_enabled_ = true;
#endif
	}

	// fxcoder/mki#31
	// hack for 4's bug: disabling of showing indicator on period affects only OnCalculate
	// should be checked in every event handler except OnCalculate
	static bool is_enabled()
	{
#ifdef __MQL4__
		return(is_enabled_);
#else
		return(true);
#endif
	}

};

#ifdef __MQL4__
bool CTimeframeUtil::is_enabled_ = false;
#endif

CTimeframeUtil _tf;
