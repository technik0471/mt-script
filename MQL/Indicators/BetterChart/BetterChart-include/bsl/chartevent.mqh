/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// OnChartEvent helper. BSL+E. © FXcoder

#property strict

class CChartEvent
{
private:

	static int    id_;
	static long   lparam_;
	static double dparam_;
	static string sparam_;


public:

	static void update(const int id, const long &lparam, const double &dparam, const string &sparam)
	{
		id_ = id;
		lparam_ = lparam;
		dparam_ = dparam;
		sparam_ = sparam;
	}

	/* Handlers */

	static bool is_click_event()
	{
		return(id_ == CHARTEVENT_CLICK);
	}
	
	static bool is_chart_change_event()
	{
		return(id_ == CHARTEVENT_CHART_CHANGE);
	}

	static bool is_key_down_event()
	{
		return(id_ == CHARTEVENT_KEYDOWN);
	}

	static bool is_key_down_event(ushort key)
	{
		return(is_key_down_event() && (key == lparam_));
	}

	/* Parameters */

	static ushort key()
	{
		return(ushort(lparam_));
	}

};

int      CChartEvent::id_     = 0;
long     CChartEvent::lparam_ = 0;
double   CChartEvent::dparam_ = 0.0;
string   CChartEvent::sparam_ = "";

CChartEvent _chartevent;
