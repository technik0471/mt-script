/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

/*
BSL+E. © FXcoder

Including all files.

The library is not intended for separate distribution and use.
Any function in the next version may be excluded or changed.

Last check: 4.1220 / 5.2190
*/

#property strict

// indentation shows inheritance

#include "std/chart.mqh"

#include "chartevent.mqh"

#include "ptr.mqh"
#include "s.mqh"
#include "tf.mqh"
