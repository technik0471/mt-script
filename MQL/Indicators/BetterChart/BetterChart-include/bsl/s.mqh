/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Sugar. BSL+E. © FXcoder

#property strict

#define _E_PROP_GET(T, N, P) T    N() const  { return((T)get(P)); }
#define _E_PROP_SET(T, N, P) bool N(T value) { return(set(P, value)); }
