/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Smart Autoscroll. © FXcoder

#property strict

#include "../bsl/all.mqh"

class CSmartAutoscroll
{
protected:

	CChart *chart_;
	int  prev_scale_;
	int  prev_last_visible_bar_;


public:

	void CSmartAutoscroll(long chart_id):
		chart_(new CChart(chart_id))
	{
		prev_scale_ = chart_.scale();
		prev_last_visible_bar_ = chart_.last_visible_bar();
	}

	void ~CSmartAutoscroll()
	{
		_ptr.safe_delete(chart_);
	}

	void chart_event()
	{
		if (!_chartevent.is_chart_change_event())
			return;

		int last_visible_bar = chart_.last_visible_bar();
		int scale = chart_.scale();
		
		// Включить автосдвиг, если в текущей ситуации виден последний бар, либо если
		// был изменён масштаб графика и при этом раньше последний бар был видим.
		bool enable_autoscroll = (last_visible_bar < 0) || ((prev_scale_ != scale) && (prev_last_visible_bar_ < 0));
		chart_.autoscroll(enable_autoscroll);
		
		prev_last_visible_bar_ = last_visible_bar;
		prev_scale_ = scale;
	}

};
