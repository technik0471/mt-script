/*
Copyright 2019 FXcoder

This file is part of BetterChart.

BetterChart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BetterChart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with BetterChart. If not, see
http://www.gnu.org/licenses/.
*/

// Коды доступных в MT клавиш (обычная клавиатура). © FXcoder
// В отличие от VK_*, здесь только те клавиши, которые работают в MT. Коды совпадают.
// Кроме того, исключены клавиши со состоянием (*Lock) и клавиша Windows

#property strict

enum ENUM_MTKEY
{
	MTKEY_NONE                  = 0,   // None
	MTKEY_ESC                   = 27,  // Esc

#ifdef __MQL5__
	MTKEY_F2                    = 113, // F2
#endif

	MTKEY_F5                    = 116, // F5

	MTKEY_GRAVE_ACCENT          = 192, // ` ~
	MTKEY_1                     = '1', // 1
	MTKEY_2                     = '2', // 2
	MTKEY_3                     = '3', // 3
	MTKEY_4                     = '4', // 4
	MTKEY_5                     = '5', // 5
	MTKEY_6                     = '6', // 6
	MTKEY_7                     = '7', // 7
	MTKEY_8                     = '8', // 8
	MTKEY_9                     = '9', // 9
	MTKEY_0                     = '0', // 0
	MTKEY_MINUS                 = 189, // - _
	MTKEY_EQUAL                 = 187, // = +
	MTKEY_BACKSPACE             = 8,   // Backspace

	MTKEY_TAB                   = 9,   // Tab
	MTKEY_Q                     = 'Q', // q Q
	MTKEY_W                     = 'W', // w W
	MTKEY_E                     = 'E', // e E
	MTKEY_R                     = 'R', // r R
	MTKEY_T                     = 'T', // t T
	MTKEY_Y                     = 'Y', // y Y
	MTKEY_U                     = 'U', // u U
	MTKEY_I                     = 'I', // i I
	MTKEY_O                     = 'O', // o O
	MTKEY_P                     = 'P', // p P
	MTKEY_LEFT_SQUARE_BRACKET   = 219, // [ {
	MTKEY_RIGHT_SQUARE_BRACKET  = 221, // ] }
	MTKEY_ENTER                 = 13,  // Enter
	MTKEY_A                     = 'A', // a A
	MTKEY_S                     = 'S', // s S
	MTKEY_D                     = 'D', // d D
	MTKEY_F                     = 'F', // f F
	MTKEY_G                     = 'G', // g G
	MTKEY_H                     = 'H', // h H
	MTKEY_J                     = 'J', // j J
	MTKEY_K                     = 'K', // k K
	MTKEY_L                     = 'L', // l L
	MTKEY_SEMICOLON             = 186, // ; :
	MTKEY_APOSTROPHE            = 222, // ' "
	MTKEY_BACKSLASH             = 220, // \ |

	MTKEY_SHIFT                 = 16,  // Shift
	MTKEY_Z                     = 'Z', // z Z
	MTKEY_X                     = 'X', // x X
	MTKEY_C                     = 'C', // c C
	MTKEY_V                     = 'V', // v V
	MTKEY_B                     = 'B', // b B
	MTKEY_N                     = 'N', // n N
	MTKEY_M                     = 'M', // m M
	MTKEY_COMMA                 = 188, // , <
	MTKEY_DOT                   = 190, // . >
	MTKEY_SLASH                 = 191, // / ?
	MTKEY_SPACE                 = 32,  // Space

	MTKEY_LEFT                  = 37,  // Left
	MTKEY_RIGHT                 = 39,  // Right
	MTKEY_UP                    = 38,  // Up
	MTKEY_DOWN                  = 40,  // Down

	MTKEY_INS                   = 45,  // Insert
	MTKEY_DEL                   = 46,  // Delete
	MTKEY_HOME                  = 36,  // Home
	MTKEY_END                   = 35,  // End
	MTKEY_PAGE_UP               = 33,  // Page Up
	MTKEY_PAGE_DOWN             = 34,  // Page Down
	MTKEY_NUM_PAD_DIV           = 111, // Num Pad /
	MTKEY_NUM_PAD_MULT          = 106, // Num Pad *
	MTKEY_NUM_PAD_SUB           = 109, // Num Pad -
	MTKEY_NUM_PAD_ADD           = 107, // Num Pad +
	MTKEY_NUM_PAD_1             = 97,  // Num Pad 1
	MTKEY_NUM_PAD_2             = 98,  // Num Pad 2
	MTKEY_NUM_PAD_3             = 99,  // Num Pad 3
	MTKEY_NUM_PAD_4             = 100, // Num Pad 4
	MTKEY_NUM_PAD_5             = 101, // Num Pad 5
	MTKEY_NUM_PAD_6             = 102, // Num Pad 6
	MTKEY_NUM_PAD_7             = 103, // Num Pad 7
	MTKEY_NUM_PAD_8             = 104, // Num Pad 8
	MTKEY_NUM_PAD_9             = 105, // Num Pad 9
	MTKEY_NUM_PAD_0             = 96,  // Num Pad 0
	MTKEY_NUM_PAD_COMMA         = 110, // Num Pad ,
};
