/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Step type. © FXcoder

#property strict


enum ENUM_STEP
{
	STEP_LINEAR       = 0,  // Linear Step
	STEP_LIN_PAR      = 4,  // Linear-Parabolic Step
	STEP_PARABOLIC    = 2,  // Parabolic Step
	STEP_PAR_EXP      = 3,  // Parabolic-Exponential Step
	STEP_EXP          = 1,  // Exponential Step
};


string enum_step_to_string(const ENUM_STEP step, const bool compact = true)
{
	switch(step)
	{
		case STEP_LINEAR:          return(compact ? "L" : "lin");     // 0
		case STEP_EXP:             return(compact ? "E" : "exp");     // 1
		case STEP_PARABOLIC:       return(compact ? "P" : "par");     // 2
		case STEP_PAR_EXP:         return(compact ? "PE": "par-exp"); // 3
		case STEP_LIN_PAR:         return(compact ? "LP": "lin-par"); // 4
	}

	return("?");
}

