/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Тип градиента. © FXcoder

#property strict

enum ENUM_GRADIENT
{
	GRADIENT_RGB                = 0,  // Градиент RGB
	GRADIENT_RGB_REVERSE        = 2,  // Градиент RGB (реверс)
	GRADIENT_HSV                = 1,  // Градиент HSV
	GRADIENT_HSV_REVERSE        = 3,  // Градиент HSV (реверс)
};

bool enum_gradient_is_hsv(const ENUM_GRADIENT gradient)
{
	return((gradient == GRADIENT_HSV) || (gradient == GRADIENT_HSV_REVERSE));
}

bool enum_gradient_is_rgb(const ENUM_GRADIENT gradient)
{
	return((gradient == GRADIENT_RGB) || (gradient == GRADIENT_RGB_REVERSE));
}

bool enum_gradient_is_reverse(const ENUM_GRADIENT gradient)
{
	return((gradient == GRADIENT_RGB_REVERSE) || (gradient == GRADIENT_HSV_REVERSE));
}
