/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

/*
Класс линии индикатора.

Для совместимости с MT4 при получение свойств через именные функции читаются только ранее установленные значения. Если
в MT5 (в MT4 это невозможно) необходимо считать свойство напрямую средствами языка, используйте средства языка (функции
группы PlotIndexGet).

В отличие от старого класса Plot в этом буферы не серийные, отсчёт слева направо.

TODO:
	* базовый класс с 1 буфером и несколько наследников, основное отличие - перегрузка Init и конструкторов для использования разного числа буферов
	* класс для универсального (4/5) доступа к свечному графику с учётом подоокна (в основном окне алгоритм имитации в 4 отличается)

© FXcoder
*/

#property strict

#include "../bsl.mqh"
#include "../const/compatibility.mqh"


// abstract
class CPlot: public CBUncopyable
{
protected:

	// Индексы линий и буферов
	int plot_index_first_;
	int plot_index_count_;
	int buffer_index_first_;
	int buffer_index_count_;

	// Параметры линии
	ENUM_DRAW_TYPE draw_type_;
	uchar arrow_;
	int arrow_shift_;
	int draw_begin_;
	double empty_value_;
	string label_;
	ENUM_LINE_STYLE line_style_;
	int line_width_;
	int shift_;
	bool show_data_;

	int color_indexes_;
	CBDict<int, color> line_colors_;


public:

	int plot_index_first () const { return(plot_index_first_); };
	int plot_index_last  () const { return(plot_index_first_ + plot_index_count_ - 1); };
	int plot_index_next  () const { return(plot_index_first_ + plot_index_count_); };
	int plot_index_count () const { return(plot_index_count_); };

	int buffer_index_first () const { return(buffer_index_first_); };
	int buffer_index_last  () const { return(buffer_index_first_ + buffer_index_count_ - 1); };
	int buffer_index_next  () const { return(buffer_index_first_ + buffer_index_count_); };
	int buffer_index_count () const { return(buffer_index_count_); };

			
	// Этот конструктор вызывается в глобальных объявлениях отдельных линий или их массивов.
	// init() здесь вызывать нельзя, может спутать индексы линий.
	void CPlot():
		plot_index_first_(0),
		plot_index_count_(0),
		buffer_index_first_(0),
		buffer_index_count_(0)
	{
		init_properties();
	}

	// abstract
	virtual CPlot *init(int plot_index_first, int buffer_index_first) = 0;
	virtual CPlot *init(const CPlot &prev_plot) = 0;
	virtual int size() const = 0; // Размер буферов
	virtual CPlot *empty(int bars = -1) = 0; // -1 = все
	virtual CPlot *empty_bar(int bar) = 0;


	//todo: т.к. уже в этом классе известен диапазон номеров буферов, сделать установку соотв. свойств в цикле (например этих: shift, draw_begin)

	// Установка и получение свойств.
	// В мт4 нет функций получения значений этих свойств, поэтому необходимо их сохранить.
	// В мт5 также нельзя прочитать некоторые свойста, лучше сохранять при установке.

#ifdef __MQL4__

	virtual CPlot *arrow          (uchar           value) { arrow_         = value; SetIndexArrow(plot_index_first_, value); return(&this); }
	virtual CPlot *arrow_shift    (int             value) { arrow_shift_   = value; return(&this); } //TODO: ?
	virtual CPlot *draw_begin     (int             value) { draw_begin_    = value; SetIndexDrawBegin(plot_index_first_, value + shift_); return(&this); } //TODO: в мт4 +shift?
	virtual CPlot *draw_type      (int             value) { draw_type_     = value; SetIndexStyle(plot_index_first_, value); return(&this); }
	virtual CPlot *empty_value    (double          value) { empty_value_   = value; SetIndexEmptyValue(plot_index_first_, value); return(&this); }
	virtual CPlot *shift          (int             value) { shift_         = value; SetIndexShift(plot_index_first_, value); return(&this); } //TODO: менять draw_begin?
	virtual CPlot *show_data      (bool            value) { show_data_     = value; if (value) { SetIndexLabel(plot_index_first_, label_); } else { SetIndexLabel(plot_index_first_, ""); } return(&this); } // неточные алгоритм, предполагает установку метки перед вызовом с true //TODO: ?
	virtual CPlot *label          (string          value) { label_         = value; SetIndexLabel(plot_index_first_, value); return(&this); }
	virtual CPlot *line_style     (ENUM_LINE_STYLE value) { line_style_    = value; SetIndexStyle(plot_index_first_, -1, value); return(&this); }
	virtual CPlot *line_width     (int             value) { line_width_    = value; SetIndexStyle(plot_index_first_, -1, -1, value); return(&this); }

//todo: автоматически расширять line_colors_? см. проблему в CBicolorHistogram2::set_two_colors
	virtual CPlot *color_indexes  (int             value) { color_indexes_ = value; return(&this); }

//todo: возомжно, стоит убрать этот вариант совсем, т.к. уже привёл один раз к проблеме
	// полностью реализуются только в наследниках-имитациях
	virtual CPlot *line_color     (int index, color value) { line_colors_.set(index, value); SetIndexStyle(plot_index_first_, -1, EMPTY, EMPTY, value); return(&this); }
	virtual CPlot *line_color     (           color value) { line_colors_.set(0,     value); SetIndexStyle(plot_index_first_, -1, EMPTY, EMPTY, value); return(&this); }

#else

	virtual CPlot *arrow          (uchar           value) { arrow_         = value; return(set(PLOT_ARROW,         value)); }
	virtual CPlot *arrow_shift    (int             value) { arrow_shift_   = value; return(set(PLOT_ARROW_SHIFT,   value)); }
	virtual CPlot *draw_begin     (int             value) { draw_begin_    = value; return(set(PLOT_DRAW_BEGIN,    value)); } //TODO: +shift в 4?
	virtual CPlot *draw_type      (ENUM_DRAW_TYPE  value) { if (value < 0) return(&this); draw_type_ = value; return(set(PLOT_DRAW_TYPE, value)); } // -1 = не устанавливать (для совместимости с 4)
	virtual CPlot *empty_value    (double          value) { empty_value_   = value; return(set(PLOT_EMPTY_VALUE,   value)); }
	virtual CPlot *shift          (int             value) { shift_         = value; return(set(PLOT_SHIFT,         value)); } //TODO: соотв. менять draw_begin в 4?
	virtual CPlot *show_data      (bool            value) { show_data_     = value; return(set(PLOT_SHOW_DATA,     value)); }
	virtual CPlot *label          (string          value) { label_         = value; return(set(PLOT_LABEL,         value)); }
	virtual CPlot *line_style     (ENUM_LINE_STYLE value) { if (value < 0) return(&this); line_style_ = value; return(set(PLOT_LINE_STYLE, value)); } // -1 = не устанавливать (для совместимости с 4)
	virtual CPlot *line_width     (int             value) { if (value < 0) return(&this); line_width_ = value; return(set(PLOT_LINE_WIDTH, value)); } // -1 = не устанавливать (для совместимости с 4)

//todo: автоматически расширять line_colors_? см. проблему в CBicolorHistogram2::set_two_colors
	virtual CPlot *color_indexes  (int             value) { color_indexes_ = value; return(set(PLOT_COLOR_INDEXES, value)); }

	virtual CPlot *line_color     (int index, color value) { line_colors_.set(index, value); return(set(PLOT_LINE_COLOR, index, value)); }
	virtual CPlot *line_color     (           color value) { if (_color.is_none(value)) return(&this); line_colors_.set(0, value); return(set(PLOT_LINE_COLOR, value)); } // None = не устанавливать (для совместимости с 4)

	//TODO: line_color реализовать только в цветных линиях

#endif

	virtual uchar            arrow          () { return( arrow_         ); }
	virtual int              arrow_shift    () { return( arrow_shift_   ); }
	virtual int              draw_begin     () { return( draw_begin_    ); }
	virtual ENUM_DRAW_TYPE   draw_type      () { return( draw_type_     ); }
	virtual double           empty_value    () { return( empty_value_   ); }
	virtual int              shift          () { return( shift_         ); }
	virtual bool             show_data      () { return( show_data_     ); }
	virtual string           label          () { return( label_         ); }
	virtual ENUM_LINE_STYLE  line_style     () { return( line_style_    ); }
	virtual int              line_width     () { return( line_width_    ); }

	virtual int              color_indexes  () { return( color_indexes_ ); }
	virtual color            line_color     (int index = 0) { return( line_colors_[index] ); }


protected:

	// Инициализация свойств
	//TODO: Проверить, какие значения заданы по умолчанию для линии, и указать их здесь.
	//TODO: Либо проинициализировать все свойства явно здесь своими значениями. (вряд ли хорошая идея)
	//TODO: В любом случае empty_value_ должно быть изначально = EMPTY_VALUE.
	//TODO: возможно, следует вызывать в каждом варианте init()
	//TODO: возможно, init() следует убрать из публичных
	void init_properties()
	{
		draw_type_ = DRAW_LINE; // не устанавливать
		arrow_ = 0;
		arrow_shift_ = 0;
		draw_begin_ = 0;
		empty_value_ = EMPTY_VALUE;
		label_ = "";
		line_style_ = STYLE_SOLID;
		line_width_ = 1;
		shift_ = 0;
		show_data_ = true;

		color_indexes_ = 0;
		line_colors_.clear();
		line_colors_.set(0, clrNONE);
	}


#ifndef __MQL4__
	// Универсальные функции доступа к свойствам (только мт5).

	CPlot *set(ENUM_PLOT_PROPERTY_INTEGER property_id, int modifier, int value)
	{
		::PlotIndexSetInteger(plot_index_first_, property_id, modifier, value);
		return(&this);
	}

	CPlot *set(ENUM_PLOT_PROPERTY_INTEGER property_id, int value)
	{
		::PlotIndexSetInteger(plot_index_first_, property_id, value);
		return(&this);
	}

	CPlot *set(ENUM_PLOT_PROPERTY_DOUBLE  property_id, double value)
	{
		::PlotIndexSetDouble(plot_index_first_, property_id, value);
		return(&this);
	}

	CPlot *set(ENUM_PLOT_PROPERTY_STRING  property_id, string value)
	{
		::PlotIndexSetString(plot_index_first_, property_id, value);
		return(&this);
	}

	int get(ENUM_PLOT_PROPERTY_INTEGER property_id, int modifier = 0)
	{
		return(::PlotIndexGetInteger(plot_index_first_, property_id, modifier));
	}

#endif

	// true if both are valid
	static bool validate_two_colors(color &bull_color, color &bear_color)
	{
		color new_bull_color = _color.validate(bull_color, _chart.color_chart_up());
		color new_bear_color = _color.validate(bear_color, _chart.color_chart_down());
		bool res = (new_bull_color == bull_color) && (new_bear_color == bear_color);
		
		// если цвета графика для свечей совпадают (по умолчанию это так), то затенить немного медвежью
		if (_color.is_none(bull_color) && _color.is_none(bear_color) && (new_bull_color == new_bear_color))
			new_bear_color = _color.mix(new_bear_color, _chart.color_background(), 0.333);

		bull_color = new_bull_color;
		bear_color = new_bear_color;
		return(res);
	}

};

