/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Индикаторные функции. © FXcoder

#property strict

#include "../bsl.mqh"

//todo: deprecate indicator_value*

#ifdef __MQL4__

double indicator_value(double value, double empty_value = EMPTY_VALUE)
{
	// В MT4 отсутствие результата равно 0.
	// На некоторых индикаторах применять нельзя, так как они и штатно, и при ошибке возвращают 0,
	// этот случай здесь никак не обрабатывается. Есть смысл вообще не использовать встроенные
	// индикаторы в MT4.
	if (value == 0)
		return(empty_value);

	return(value);
}

#else

double indicator_value_by_bar(int handle, int bar, double empty_value = EMPTY_VALUE)
{
	return(indicator_value_by_bar(_Symbol, _Period, handle, bar, empty_value));
}

double indicator_value_by_bar(string symbol, ENUM_TIMEFRAMES timeframe, int handle, int bar, double empty_value = EMPTY_VALUE)
{
	if (handle == INVALID_HANDLE)
		return(empty_value);

	// Определить время бара для символа графика
	datetime times[];

	if (CopyTime(symbol, timeframe, bar, 1, times) != 1)
		return(empty_value);

	return(indicator_value_by_time(handle, times[0], empty_value));
}

double indicator_value_by_time(int handle, datetime bar_time, double empty_value = EMPTY_VALUE)
{
	if (handle == INVALID_HANDLE)
		return(empty_value);

	double values[];

	if (CopyBuffer(handle, 0, bar_time, 1, values) != 1)
		return(empty_value);

	return(values[0]);
}

double indicator_buffer_value_by_time(int handle, int buffer_number, datetime bar_time, double empty_value = EMPTY_VALUE)
{
	if (handle == INVALID_HANDLE)
		return(empty_value);

	double values[];

	if (CopyBuffer(handle, buffer_number, bar_time, 1, values) != 1)
		return(empty_value);

	return(values[0]);
}

bool indicators_release(const int &handles[])
{
	bool result = true;

	for (int i = 0, count = ArraySize(handles); i < count; i++)
		result = IndicatorRelease(handles[i]) && result; //TODO: учёт неинициализированных
		
	return(result);
}

#endif


int get_bars_to_calculate(int rates_total, string symbol, int prev_calculated, int max_bars, int min_bars, int reserved_bars)
{
	// нерассчитанные бары
	int bars_to_calculate = rates_total - prev_calculated;

	// сначала проверим минимум, т.к. он необязателен
	// минимум может быть 0 (например, при расчёте по ценам открытия)
	if (bars_to_calculate < min_bars)
		bars_to_calculate = min_bars;

	// проверить максимум
	if ((max_bars > 0) && (bars_to_calculate > max_bars))
		bars_to_calculate = max_bars;

	// максимум баров инструмента
	int rates_max_bars = Bars(symbol, _Period);
	int terminal_max_bars = _terminal.max_bars();
	
	if (rates_max_bars > terminal_max_bars)
		rates_max_bars = terminal_max_bars;

	int symbol_rates_total = _sym.is_current(symbol) ? rates_total : rates_max_bars;// Bars(symbol, _Period); //TODO: Bars в 5 может давать больше значение, чем доступно через CopyRates

	if (bars_to_calculate > symbol_rates_total)
		bars_to_calculate = symbol_rates_total;
		
	// проверить резерв
	if (symbol_rates_total - bars_to_calculate < reserved_bars)
		bars_to_calculate = symbol_rates_total - reserved_bars;

	return(bars_to_calculate >= 0 ? bars_to_calculate : 0);
}


// Возвращает название скрипта с возможным обрезанием суффикса с номером версии (Index-v7 -> Index).
string get_script_name(bool remove_version)
{
	string name = _mql.program_name();

	if (!remove_version)
		return(name);

	int trim_pos = -1;
	int pos = 0;

	while (pos > -1)
	{
		pos = StringFind(name, "-v", pos + 1); // можно начинать с символа 1, т.к. иначе имя индикатора пусто, чего не должно быть
		
		if (pos != -1)
			trim_pos = pos;
	}

	if (trim_pos != -1)
		name = StringSubstr(name, 0, trim_pos);

	return(name);
}

