This file is mostly translated using automatic translators. If you find an error, report it through [Issues](https://gitlab.com/fxcoder/mt-script/issues).

# MetaTrader Scripts

Unless otherwise specified, the code is available for both MT4 and MT5.

Further, under the `MQL` folder, the ` MQL4` or `MQL5` folder is assumed depending on the version of the terminal.

Scripts for which there are versions for MT4 and MT5 can be identical in content. You have to use separate files, because MetaTrader recognizes the MQL version exclusively by the file extension.

## Installation

The indicator or script code usually consists of a pair of main files, one for MQL4 and MQL5 (with extensions .mq4 and .mq5), and folders with included files (`...- include`).

To install, just copy the folder containing these files and the folder in one place, for indicators it is `MQL / Indicators`. No need to move the `...- include` folder to the `MQL/Includes` folder, because the code uses relative paths, and this method will not work.

Folder with included files will not interfere in MetaTrader, because it does not have the main indicator code, and it will not appear in the folder structure in the Navigator. You can also not delete the "extra" main file from another version of MQL, this file will also be ignored in MT Navigator.

For example, to install the VP indicator, just copy the `MQL/Indicators/VP` folder from this repository to the `MQL4/Indicators/VP` and/or `MQL5/Indicators/VP` folders. Again, the VP.mq4 and VP.mq5 files and the `VP-include` folder must be in the same folder. In order not to have an extra level folder in the Navigator, you can copy the contents of `MQL/Indicators/VP` directly to `MQL5/Indicators`.

Below are shown options for the correct location of files and folders after installing two indicators - VP and Index.

As is (in the repository) for MQL5:

```ini
[MQL5]
    [Indicators]
        [VP]
            [VP-include]
            VP.mq4
            VP.mq5
        [Index]
            [Index-include]
            Index.mq4
            Index.mq5
```

The `.mq4` files could be removed here, but they will not interfere, you can leave it.

Without an extra folder in the Navigator for 4 and 5:

```ini
...
[MQL4]
    [Indicators]
        [Index-include]
        [VP-include]
        Index.mq4
        Index.mq5
        VP.mq4
        VP.mq5
...
[MQL5]
    [Indicators]
        [Index-include]
        [VP-include]
        Index.mq4
        Index.mq5
        VP.mq4
        VP.mq5
```

To find out where the `MQL4` or` MQL5` folder is located in MetaTrader or MetaEditor, you can call the menu `File/Open Data Folder`.

## Scripts

### ColorManager

A script to control the color scheme of the chart. More details: <https://www.fxcoder.ru/2012/08/colormanager-script.html>.

At the first start, a file with a small set of color schemes will be created. Color schemes are located in the file `MQL/Files/ColorManager/colors.ini`.

### FindVL

The script shows the testing of the maximum volume levels of the volume profile distributions. More details: <https://www.fxcoder.ru/2019/11/findvl-script.html>.

### ScrollMaster

The script synchronizes the offset of all graphs in the terminal with the one on which it is running. More details: <https://www.fxcoder.ru/2019/11/scrollmaster.html>.

## Indicators

### BetterChart

The indicator is designed to improve the functions of the standard chart. More details: <https://www.fxcoder.ru/2019/12/betterchart-indicator.html>

### Chart

An indicator for displaying a chart in the form of candles in an additional window. More details: <https://www.fxcoder.ru/2011/06/chart-indicator.html>.

### Index

Index based on geometric mean. More details: <https://www.fxcoder.ru/2011/06/index-indicator.html>.

### MultiStoch

Multiple Stochastic indicators in one window. More details: <https://www.fxcoder.ru/2012/05/multistoch-indicator.html>.

### VP

Distribution of transactions by price levels at a given time interval. More details: <https://www.fxcoder.ru/2009/09/volume-profile-indicator.html>.

<!--

### EA-Line и EA-Line0

Набор индикаторов для построения графиков по данным торговых роботов при визуальном тестировании в MetaTrader. Подробнее: <https://www.fxcoder.ru/2013/07/ea-line-indicator.html>.

### MA

Скользящая средняя (Moving Average) с индикацией касания цены. Подробнее: <https://www.fxcoder.ru/2011/06/ma-indicator.html>.

### StdScore

Расчет стандартной оценки и вывод ее в виде свечей. Подробнее: <https://www.fxcoder.ru/2013/07/stdscore-indicator.html>. Только MT4.
-->
